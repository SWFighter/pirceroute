#ifndef __COMM_H__
#define __COMM_H_
#include<stdlib.h>
#include<stdio.h>
#include "appconfig.h"

#define DebugColourNum                10


typedef union
{
    unsigned char buf[4];
    float              data;
}FloatUnionDef;

extern const unsigned char DebugColour[DebugColourNum][32];

int msget_method(const char *funname);

void system_time_get(struct tm **date);

int system_info_show(void);

int system_info_file_update(void);

int run_shell_modlue(char *cmd,char *buf,long size);

int get_thread_policy( pthread_attr_t *attr );

void set_thread_policy( pthread_attr_t *attr,  int policy );

void show_thread_priority( pthread_attr_t *attr, int policy );

long buf_u8_to_u32(char *buf);

long u8_to_u32(unsigned char h1,unsigned char h0,unsigned char l1,unsigned char l2);

int u8_to_u16(unsigned char h,unsigned char l);

float u8_to_float(unsigned char h1,unsigned char h0,unsigned char l1,unsigned char l0);


void float_to_u8(unsigned char *bufch,float raw);

#endif

