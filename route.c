#include "route.h"
#include "nrfTRX.h"
#include "routlist.h"

static int nrf_16byte_mode(void);
static int msg_send_mail(MsgSendMailDef_p msg);
static int msg_route_send(NrfMsgDef *msg,NrfMsgDef *recv,int direction,int wait);


static int SendMsg;


//static int RouteWorkMode = ROUTE;
static int RouteWorkMode = CCOL;
static int recvMsgThreadWork = 0;
static int DevMac = 0;

char testNum = 0;
void nrf_test_mail(void)
{
  NrfDevDef *nrf_dev;
  int result;

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);
  get_nrf_ap_config(nrf_dev);
  printf("ch = %d\n",nrf_dev->ch);
  printf("len = %d\n",nrf_dev->data_len);
  printf("wait_time =  %d\n",nrf_dev->wait_time);
  //设置报文
  nrf_dev->wait_time = 0;
  //nrf_dev->data_len = 16;

  nrf_dev->src_addr[2] = 1;
  nrf_dev->dst_addr[2] = 1;//设置地址

  //设置应答数据
  //nrf_dev->cmd = NRF_16B_MODE;

  //获取发送与接收数据缓存资源
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  {
    //监听到nrf 数据包
    int i;
    for(i=0;i<nrf_dev->data_len;i++)
    {
      nrf_dev->send_buf[i] = testNum;
    }
    printf("\nRecv <<<:");
  }
  testNum++;

  {
    //监听到nrf 数据包
    int i;
    for(i=0;i<nrf_dev->data_len;i++)
    {
      printf("%x ",nrf_dev->send_buf[i]);
    }
    printf("\nRecv <<<:");
  }

  nrf_send_mail(nrf_dev);

  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);
}

int ap_msg_auth_ack(NrfMsgDef_p msg,int dst)
{
  NrfDevDef *nrf_dev;
  int result;
  int selfIP = 0;
  //int fromIp;

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);
  get_nrf_ap_config(nrf_dev);//获取AP的配置

  set_nrfdev_dstaddr(nrf_dev,dst);
  selfIP = get_self_nrf_srcaddr();
  set_nrfdev_srcaddr(nrf_dev, selfIP);
  nrf_dev->wait_time = 0;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;

  /* if(msg->buf[4] != nrf_dev->ch)
     {
     printf("ch != %d\n",nrf_dev->ch);
     free(nrf_dev);
     return -1;
     }*/
  //申请接收与发送数据空间
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);
  memcpy(nrf_dev->send_buf,msg->buf,nrf_dev->data_len);
  nrf_dev->send_buf[0] = nrf_dev->src_addr[NRF_ADDR_LEN-2];
  nrf_dev->send_buf[1] = nrf_dev->src_addr[NRF_ADDR_LEN-1];
  nrf_dev->send_buf[MSG_CMD] = MSG_AP_Auth_Ack;
  /*nrf_dev->send_buf[3] = msg->buf[3]+1;
    nrf_dev->send_buf[4] = auth;
    nrf_dev->send_buf[5] = (num & 0xff00)>>8;
    nrf_dev->send_buf[6] = (num & 0x00ff);
    nrf_dev->send_buf[7] = (ip & 0xff00)>>8;
    nrf_dev->send_buf[8] = (ip &0x00ff);
    */

  nrf_send_mail(nrf_dev);

  if(nrf_dev->recv_buf[MSG_CMD] == NRF_SEND_ERROR)
  {
    result = -1;
  }
  else
  {
    result = 0;
  }


  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);

  return result;
}

long mac_buf_to_long(RxNrfMsgDef *msg)
{
  long mac=0;

  mac = msg->buf[5]*16777216+msg->buf[6]*65536+msg->buf[7]*256+msg->buf[8];
  return mac;
}



static void route_auth_req_process(RxNrfMsgDef *raw)
{
  int ch = 0;
  int dstip = 0;
  long mac;
  int reqType = 0;
  NrfDevDef nrfconfig;
  NrfMsgDef_p  msg;
  int result;
  int selfIp = 0;
  DevInfoDef_p selfInfo;

  selfInfo = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(selfInfo);

  selfIp = get_self_nrf_srcaddr();
  dev_list_node_get(selfIp,selfInfo);

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  
  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);

  ch = msg->routeAuthReq.ch;                                   //获取通道号
  mac = u8_to_u32(msg->routeAuthReq.macH1,msg->routeAuthReq.macH0,msg->routeAuthReq.macL1,msg->routeAuthReq.macL0);     //获取MAC地址
  reqType  = msg->routeAuthReq.type;                          //请求类型
  dstip = u8_to_u16(msg->routeAuthReq.ipH,msg->routeAuthReq.ipL);
  printf("Auth From: CH = %d ",ch);
  printf("MAC = %d ",mac);
  printf("ReqType = %d ",reqType);
  printf("FromIP = %d ",dstip);
  printf("RouteIP = %d \n",selfIp);

  get_nrf_ap_config(&nrfconfig);
  if(ch != nrfconfig.ch)
  {
    printf("wap ch != device ch\n");
    free(msg);
    return ;
  }

  //判读自己是否为集中器
  if(RouteWorkMode == CCOL)
  {
    //DevListDef *dev;
    //查询设备列表    
    int dhcp;
    int linkNum;
    int devSum;
    DevInfoDef  info;

    printf("My Is CCOL\n");

    dhcp = dev_list_dhcp_get(mac,&info);
    printf("dhcp = %d\n",dhcp);
    if(dhcp != -1)
    {      
      dev_list_show_node(dhcp);
      if((info.auth == Auth_None) || (info.auth == Auth_Ask))//设备还没有鉴权的
      {
        if(reqType == 1)//鉴权
        {
          printf("auth finsh\n");
          info.auth = Auth_Finsh;
          info.transfer.flag = TRANSFE_NONE;
          info.transfer.result = 0;
        }
        else
        {
          printf("auth ask\n");
          info.auth = Auth_Ask;
        }

        linkNum = dev_list_num_get();
        devSum = dev_list_sum_get();

        memset(msg->buf,0,LAN_MSG_FIX_LEN);
        msg->routeAuthAck.auth = 1;
        msg->routeAuthAck.cmd =  MSG_AP_Auth_Ack;
        msg->routeAuthAck.depthRoute = 0;
        msg->routeAuthAck.devNUmH = (linkNum & 0xff00)>>8;
        msg->routeAuthAck.devNumL = (linkNum & 0x00ff);
        msg->routeAuthAck.dhcpH= (dhcp & 0xff00)>>8;
        msg->routeAuthAck.dhcpL= (dhcp & 0x00ff);
        msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
        msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
        msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
        msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
        msg->routeAuthAck.devType = info.devtype;


        result = ap_msg_auth_ack(msg,dstip);
        if(result == 0)//鉴权应答发送成功
        {
          if(reqType == 1)//只保存鉴权
          {
            //设置路由表
            info.route[0] = selfIp;
            info.route[1] = dstip;
          }

          memset(&info.transfer,0,sizeof(transferCrlDef));//清除流传输控制块
          dev_list_node_set(dhcp,&info);
          dev_list_show();
        }

      }
      else//已经鉴权成功
      {
        linkNum = dev_list_num_get();
        devSum = dev_list_sum_get();

        memset(msg->buf,0,LAN_MSG_FIX_LEN);
        if(info.auth == Auth_Finsh)
        {
          msg->routeAuthAck.auth = 3;
          info.flag.putDevPrice = 1;//重新下发价格
          info.flag.putDevConfig = 1;//重新下发配置
          dev_list_node_set(dhcp,&info);
        }
        else if(info.auth == Auth_No)//没有权限
        {
          msg->routeAuthAck.auth = 0;
        }
        else
        {
          msg->routeAuthAck.auth = 1;
        }
        
        msg->routeAuthAck.cmd =  MSG_AP_Auth_Ack;
        msg->routeAuthAck.depthRoute = 0;
        msg->routeAuthAck.devNUmH = (linkNum & 0xff00)>>8;
        msg->routeAuthAck.devNumL = (linkNum & 0x00ff);
        msg->routeAuthAck.dhcpH= (dhcp & 0xff00)>>8;
        msg->routeAuthAck.dhcpL= (dhcp & 0x00ff);
        msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
        msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
        msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
        msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
        msg->routeAuthAck.devType = info.devtype;

        ap_msg_auth_ack(msg,dstip);
      }
    }
    else//这个设备没有在列表中
    {
      printf("none find device %d\n",mac);
      msg->routeAuthAck.auth = 0;
      msg->routeAuthAck.cmd =  MSG_AP_Auth_Ack;
      msg->routeAuthAck.depthRoute = 0;
      msg->routeAuthAck.devNUmH = (linkNum & 0xff00)>>8;
      msg->routeAuthAck.devNumL = (linkNum & 0x00ff);
      msg->routeAuthAck.dhcpH= (dhcp & 0xff00)>>8;
      msg->routeAuthAck.dhcpL= (dhcp & 0x00ff);
      msg->routeAuthAck.devType = info.devtype;
      
      ap_msg_auth_ack(msg,dstip);
    }
  }
  else//自身是路由器设备
  {
    result = unAuth_list_find_mac(mac);
    if(result != -1)//如果这个设备没有权限直接拒绝请求
    {
      printf("This Mac=%d Device None Permission !!!\n",mac);
      msg->routeAuthAck.auth = 0;
      msg->routeAuthAck.cmd = MSG_AP_Auth_Ack;
      msg->routeAuthAck.depthRoute = 0;
      msg->routeAuthAck.devNUmH = 0;//连接的数量
      msg->routeAuthAck.devNumL = 0;
      msg->routeAuthAck.dhcpH = 0;
      msg->routeAuthAck.dhcpL = 0;
      msg->routeAuthReq.ipH = (selfIp & 0xff00) >> 8;
      msg->routeAuthReq.ipL = (selfIp & 0x00ff);
      msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
      msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
      msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
      msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
      
      ap_msg_auth_ack(msg,dstip);//让设备等等下次鉴权
    }
    else
    {
      int linkNum;
      int devSum;
      int dhcp;

      DevInfoDef  info;//存储auth设备信息
      printf("My is Route\n");

      dhcp = dev_list_dhcp_get(mac,&info);
      if(dhcp != -1)//设备列表中已经记录了这个设备
      {
        dev_list_show_node(dhcp);

        if(info.auth == Auth_No)//拒绝鉴权(路由器设备中)
        {
          msg->routeAuthAck.auth = 1;
          msg->routeAuthAck.cmd = MSG_AP_Auth_Ack;
          msg->routeAuthAck.depthRoute = info.depthRoute;
          msg->routeAuthAck.devNUmH = 0;//连接的数量
          msg->routeAuthAck.devNumL = 0;
          msg->routeAuthAck.dhcpH = 0;
          msg->routeAuthAck.dhcpL = 0;
          msg->routeAuthReq.ipH = (selfIp & 0xff00) >> 8;
          msg->routeAuthReq.ipL = (selfIp & 0x00ff);
          msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
          msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
          msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
          msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
          msg->routeAuthAck.devType = info.devtype;
        }
        else//允许鉴权
        {
          linkNum = dev_list_num_get();
          devSum = dev_list_sum_get();
          msg->routeAuthAck.auth = 3;
          msg->routeAuthAck.cmd = MSG_AP_Auth_Ack;
          msg->routeAuthAck.depthRoute = info.depthRoute;
          msg->routeAuthAck.devNUmH = (linkNum & 0xff00)>>8;//连接的数量
          msg->routeAuthAck.devNumL = (linkNum & 0x00ff);
          msg->routeAuthAck.dhcpH = (dhcp & 0xff00)>>8;//dhcp;
          msg->routeAuthAck.dhcpL = (dhcp & 0x00ff);
          msg->routeAuthAck.ipH = (selfIp & 0xff00) >> 8;
          msg->routeAuthAck.ipL = (selfIp & 0x00ff);
          msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
          msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
          msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
          msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
          msg->routeAuthAck.devType = info.devtype;

          if(reqType == 1)//真正鉴权
          {
              info.auth = Auth_Finsh;
              info.route[info.depthRoute+1] = dstip;
              info.transfer.flag = TRANSFE_NONE;
              info.transfer.result = 0;
          }
          else
          {
            info.auth = Auth_Ask;
          }
        }
        ap_msg_auth_ack(msg,dstip);//让设备等等下次鉴权
        dev_list_node_set(dhcp,&info);
      }
      else//设备列表没有记录这个设备
      {
        //应答设备进入等待鉴权
        linkNum = dev_list_num_get();
        devSum = dev_list_sum_get();
        msg->routeAuthAck.auth = 2;
        msg->routeAuthAck.cmd = MSG_AP_Auth_Ack;
        msg->routeAuthAck.depthRoute = 0;
        msg->routeAuthAck.devNUmH = 0;//连接的数量
        msg->routeAuthAck.devNumL = 0;
        msg->routeAuthAck.dhcpH = 0;
        msg->routeAuthAck.dhcpL = 0;
        msg->routeAuthReq.ipH = (selfIp & 0xff00) >> 8;
        msg->routeAuthReq.ipL = (selfIp & 0x00ff);
        msg->routeAuthAck.MacH1 = (mac & 0xff000000)>>24;
        msg->routeAuthAck.MacH0 = (mac & 0x00ff0000)>>16;
        msg->routeAuthAck.MacL1 = (mac & 0x0000ff00)>>8;
        msg->routeAuthAck.MacL0 = (mac & 0x000000ff)>>0;
        msg->routeAuthAck.devType = info.devtype;

        ap_msg_auth_ack(msg,dstip);//让设备等等下次鉴权

        //组织下级设备应答请求鉴权
        //MsgSendMailDef_p mail;
        NrfMsgDef_p msg,recv;

        msg = calloc(1,sizeof(MsgSendMailDef));
        OS_ASSERT(msg);

        recv = calloc(1,sizeof(MsgSendMailDef));
        OS_ASSERT(recv)

        memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝接收的原始数据到报文结构体

        msg->routeAuthReq.cmd = MSG_AP_Auth_Req;//向上级路由请求鉴权
        msg->routeAuthReq.ch = msg->routeAuthReq.ch;
        msg->routeAuthReq.macH1 = (mac & 0xff000000)>>24;
        msg->routeAuthReq.macH0 = (mac & 0x00ff0000)>>16;
        msg->routeAuthReq.macL1 = (mac & 0x0000ff00)>>8;
        msg->routeAuthReq.macL0 = (mac & 0x000000ff)>>0;
        msg->routeAuthReq.ipH = (selfIp & 0xff00) >> 8;//鉴权必须用自己的IP
        msg->routeAuthReq.ipL = (selfIp & 0x00ff);
        msg->routeAuthAck.devType = info.devtype;

        result = msg_route_send(msg,recv,-1,0);

        free(msg);
        free(recv);
        //由于集中器不一定马上应答因此发送邮件
      }
    }
  }
  //ap_msg_auth_ack(msg);
  free(msg);
  free(selfInfo);
}

static int route_auth_ack_process(RxNrfMsgDef *raw)
{
    int result;
    NrfMsgDef_p  recv;
    //DevInfoDef_p selfInfo;//路由器自己的信息
    DevInfoDef_p info;//设备的配置信息
    int                selfIp;
    int                fromMac;
    
    recv = calloc(1,sizeof(NrfMsgDef));
    OS_ASSERT(recv);

    info = calloc(1,sizeof(DevInfoDef));
    OS_ASSERT(info);

    selfIp = get_self_nrf_srcaddr();//得到自己的IP地址，方便取出自己的设备信息
    
    memcpy(recv->buf,raw->buf,LAN_MSG_FIX_LEN);
    fromMac =  u8_to_u32(recv->routeAuthAck.MacH1,recv->routeAuthAck.MacH0,recv->routeAuthAck.MacL1,recv->routeAuthAck.MacL0);

    if(RouteWorkMode == ROUTE)//路由模式
    {
        //添加设到设备列表中
        int linkNum;
        int dhcp;

        dhcp = u8_to_u16(recv->routeAuthAck.dhcpH,recv->routeAuthAck.dhcpL);
        if(recv->routeAuthAck.auth == 1 ||recv->routeAuthAck.auth == 3 )//允许鉴权
        {
          info->auth = Auth_Ask;
          info->depthRoute = recv->routeAuthAck.depthRoute+1;
          info->mac =fromMac;//mac地址
          info->devtype = recv->routeAuthAck.devType;
          info->route[info->depthRoute -1] =  u8_to_u16(recv->routeAuthAck.ipH,recv->routeAuthAck.ipL);//上级设备地址
          info->route[info->depthRoute] = selfIp;
          printf("Add Device DHCP=%d\n",dhcp);
          dev_list_node_set(dhcp,info);
          dev_list_show();
        }
        else if(recv->routeAuthAck.auth == 0)//上级路由不允许鉴权
        {
          //缓存被拒绝的设备
          add_unAuth_dev(fromMac);
        }
    }
    
    free(recv);
    free(info);
    
    return result;
}

static void ccol_test_list_create(void)
{
  int result;
  long i;
  DevInfoDef  info = {0};
  int devnum = 1000;

  //创建设备列表
  dev_list_create(devnum);

  //模拟路由器设备
  for(i=1;i<32;i++)
  {
    info.mac = 32-i;
    if(info.mac == DevMac)
    {
      info.devtype = DevWapCCOL;
      info.auth = Auth_Finsh;
    }
    else
    {
      info.devtype = DevWapRoute;
    }
    info.flag.putDevConfig= 1;
    info.flag.putDevPrice= 1;
    dev_list_node_set(i,&info);
    info.auth = Auth_None;
    info.price.data = 4.4;
  }

  //模拟价签设备
  for(i=32;i<devnum;i++)
  {
    /*if(i == devnum-2)
    {
      int packNum;

      info.mac = 37;
      info.price.data = 37;
      sprintf(info.ramConfig.data,"123456789abcd%04d\n",info.mac);
      info.ramConfig.size = strlen(info.ramConfig.data);

      packNum = info.ramConfig.size/Tran_BUF_SIZE;
      if(info.ramConfig.size%Tran_BUF_SIZE != 0)
      {
        packNum++;
      }
      info.ramConfig.crc16 = CRC16_2(packNum*Tran_BUF_SIZE,info.ramConfig.data);
      info.ramConfig.id = 1;
      info.flag.syncFinishFlag = 1;
    }
    else if(i == devnum - 5)
    {
      int packNum;

      info.mac = 38;
      info.price.data = 31;
      strcpy(info.ramConfig.data,"杨鹏利");
      info.ramConfig.size = strlen(info.ramConfig.data);

      packNum = info.ramConfig.size/Tran_BUF_SIZE;
      if(info.ramConfig.size%Tran_BUF_SIZE != 0)
      {
        packNum++;
      }
      info.ramConfig.crc16 = CRC16_2(packNum*Tran_BUF_SIZE,info.ramConfig.data);
      info.ramConfig.id = 1;
      info.flag.syncFinishFlag = 1;
    }
    else if(i == devnum - 4)
    {
      int packNum;

      info.mac = 39;
      info.price.data = 39.7;
      strcpy(info.ramConfig.data,"测试设备ramconfig data");
      info.ramConfig.size = strlen(info.ramConfig.data);

      packNum = info.ramConfig.size/Tran_BUF_SIZE;
      if(info.ramConfig.size%Tran_BUF_SIZE != 0)
      {
        packNum++;
      }
      info.ramConfig.crc16 = CRC16_2(packNum*Tran_BUF_SIZE,info.ramConfig.data);
      info.ramConfig.id = 1;
      info.flag.syncFinishFlag = 1;
    }
    else*/
    {
      int packNum;
    
      info.mac = i;//其他的mac
      info.price.data = info.mac;
      sprintf(info.ramConfig.data,"123456789abcd%04d\n",info.mac);
      info.ramConfig.size = strlen(info.ramConfig.data);

      packNum = info.ramConfig.size/Tran_BUF_SIZE;
      if(info.ramConfig.size%Tran_BUF_SIZE != 0)
      {
        packNum++;
      }
      info.ramConfig.crc16 = CRC16_2(packNum*Tran_BUF_SIZE,info.ramConfig.data);
      info.ramConfig.id = 1;
    }
    
    info.flag.putDevPrice= 1;
    info.devtype = DevPriceTag1;
    dev_list_node_set(i,&info);
  }

  //显示设备列表
  dev_list_show();

}

static void route_test_list_create(void)
{

}

static int route_thread_init(void)
{
  int result = 0;

  return result; 
}

int msg_test_process(RxNrfMsgDef *msg)
{
  NrfDevDef *nrf_dev;
  int result;
  int fromIp;

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);
  get_nrf_ap_config(nrf_dev);//获取AP的配置

  fromIp = u8_to_u16(msg->buf[0],msg->buf[1]);
  set_nrfdev_dstaddr(nrf_dev,fromIp);
  set_nrfdev_srcaddr(nrf_dev, 1);
  nrf_dev->wait_time = 0;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;

  //申请接收与发送数据空间
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  memcpy(nrf_dev->send_buf,msg->buf,nrf_dev->data_len);

  nrf_send_mail(nrf_dev);

  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);

  return result;
}

static int route_event_msg_process(RxNrfMsgDef *raw)
{
  int result;

  //解析事件类型

  return result;
}

static int stream_req_process(RxNrfMsgDef *raw)
{
  //是能是上级路由才运行
  int result;
  int fromMac;
  int fromIP;
  int streamType;
  int streamID;
  int streamPos;
  int dhcp;
  DevInfoDef_p info;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  int selfIP;//路由器自身的ip
  int surplusPack = 0;//剩余包数量
  int streamCrc16;
  int reqResult;

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  selfIP = get_self_nrf_srcaddr();//获取到自己的ip地址

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);
  fromMac = u8_to_u32(msg->streamReq.macH1,msg->streamReq.macH0,msg->streamReq.macL1,msg->streamReq.macL0);//获取MAC
  fromIP = u8_to_u16(msg->streamReq.ipH,msg->streamReq.ipL);//获取IP
  streamType = msg->streamReq.type;
  streamID = msg->streamReq.id;
  streamPos = u8_to_u16(msg->streamReq.posH,msg->streamReq.posL);

  dhcp = dev_list_dhcp_get(fromMac,info);
  if(dhcp != -1 && info->auth == Auth_Finsh)//有这个设备,并且是在线的
  {
    //if(info->flag.routeGetConfig == 0 ) //不处于下拉配置期间，才响应请求
    {
      //unsigned char *data;

      //data = info->ramConfig.data;
      if(info->ramConfig.id == streamID || streamID == 1)//续传
      {
        if(((info->ramConfig.size+Tran_BUF_SIZE)- Tran_BUF_SIZE*streamPos) > 0)//下级设备要的数据比，当前这个流还大
        {
          surplusPack = (info->ramConfig.size - Tran_BUF_SIZE*streamPos)/Tran_BUF_SIZE;
          if((info->ramConfig.size - Tran_BUF_SIZE*streamPos)%Tran_BUF_SIZE)
          {
            surplusPack += 1;
          }
          streamCrc16 = info->ramConfig.crc16;
          reqResult = 0;
          //info->transfer.flag = TRANSFE_DATA_TX;//进入传输状态
          dev_list_node_set(dhcp, info);//下推配置事件传递成功。
        }
        else//异常,回一个重传
        {
          OS_ASSERT(NULL);
        }
      }
      else//重传
      {
        streamPos = 0;
        surplusPack = (info->ramConfig.size - Tran_BUF_SIZE*streamPos)/Tran_BUF_SIZE;
        if((info->ramConfig.size - Tran_BUF_SIZE*streamPos)%Tran_BUF_SIZE > 0)
        {
          surplusPack += 1;
        }
        streamCrc16 = info->ramConfig.crc16;
        reqResult = 1;
        //info->transfer.flag = TRANSFE_DATA_TX;//进入传输状态
        dev_list_node_set(dhcp, info);//下推配置事件传递成功。
      }
    }
    //else//没有同步配置信息
    {
      //reqResult = 2;
    }
    memset(msg->buf,0,LAN_MSG_FIX_LEN);
    msg->streamAck.cmd = MSG_Stream_Ack;
    msg->streamAck.ipH = (selfIP & 0xff00)>>8;
    msg->streamAck.ipL = (selfIP & 0x00ff);
    msg->streamAck.packNumH = (surplusPack & 0xff00)>>8;
    msg->streamAck.packNumL = (surplusPack & 0x00ff);
    msg->streamAck.crc16H = (streamCrc16 & 0xff00)>>8;
    msg->streamAck.crc16L = (streamCrc16 & 0x00ff);
    msg->streamAck.error = reqResult;
    msg->streamAck.id = info->ramConfig.id;//用于区分这个数据是否被改变
    msg->streamAck.macH1 = (fromMac & 0xff000000)>>24;
    msg->streamAck.macH0 = (fromMac & 0x00ff0000)>>16;
    msg->streamAck.macL1= (fromMac & 0x0000ff00)>>8;
    msg->streamAck.macL0 = (fromMac & 0x000000ff)>>0;
    //应答下级设备
    //msg_route_send(msg,recv,1,0);
    result = msg_send_dev(msg, recv, info->route[info->depthRoute+1], 0);
    if(result != 0)
    {
        printf("MSG_Stream_Ack Fail\n");
    }
    //info->flag.putDevConfig = 0;
    //dev_list_node_set(dhcp, info);//下推配置事件传递成功。
  }
  else//异常
  {
    //OS_ASSERT(NULL);
    printf("fromIP =%d None Find\n",fromIP);
    memset(msg->buf,0,LAN_MSG_FIX_LEN);
    msg->streamAck.cmd = MSG_Stream_Ack;
    msg->streamAck.ipH = (selfIP & 0xff00)>>8;
    msg->streamAck.ipL = (selfIP & 0x00ff);
    msg->streamAck.packNumH = (surplusPack & 0xff00)>>8;
    msg->streamAck.packNumL = (surplusPack & 0x00ff);
    msg->streamAck.crc16H = (streamCrc16 & 0xff00)>>8;
    msg->streamAck.crc16L = (streamCrc16 & 0x00ff);
    msg->streamAck.error = 2;
    msg->streamAck.id = info->ramConfig.id;//用于区分这个数据是否被改变
    msg->streamAck.macH1 = (fromMac & 0xff000000)>>24;
    msg->streamAck.macH0 = (fromMac & 0x00ff0000)>>16;
    msg->streamAck.macL1= (fromMac & 0x0000ff00)>>8;
    msg->streamAck.macL0 = (fromMac & 0x000000ff)>>0;
    //应答下级设备
    msg_send_dev(msg, recv,fromIP, 0);
  }

  free(recv);
  free(msg);
  free(info);

  return result;
}


static int stream_ack_process(RxNrfMsgDef *raw)
{
  int result;
  DevInfoDef_p info;
  NrfMsgDef_p  msg;
  int selfIP;
  int srcIP;
  int srcMac;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();
  //srcIP = u8_to_u16(msg->streamAck.ipH,msg->streamAck.ipL);
  srcMac = u8_to_u32(msg->streamAck.macH1,msg->streamAck.macH0,msg->streamAck.macL1,msg->streamAck.macL0);
  srcIP = dev_list_dhcp_get(srcMac,info);
  if(result != -1)//这个路由上能找到这个设备
  {
    if(msg->streamAck.error == 0)
    {
      info->transfer.crc16 = u8_to_u16(msg->streamAck.crc16H,msg->streamAck.crc16L);
      info->transfer.packNum = u8_to_u16(msg->streamAck.packNumH,msg->streamAck.packNumL);
      info->transfer.flag = TRANSFE_DATA_RX;//进入传输状态
      info->transfer.id = msg->streamAck.id;
      printf("包个数%d,CRC16=%d id=%d\n",info->transfer.packNum,info->transfer.crc16,info->transfer.id);
      dev_list_node_set(srcIP, info);
      dev_list_show_node(srcIP);
    }
    else if(msg->streamAck.error == 1)
    {
      info->transfer.crc16 = u8_to_u16(msg->streamAck.crc16H,msg->streamAck.crc16L);
      info->transfer.packNum = u8_to_u16(msg->streamAck.packNumH,msg->streamAck.packNumL);
      info->transfer.flag = TRANSFE_DATA_RX;//进入传输状态
      info->transfer.id = msg->streamAck.id;
      info->transfer.curPos = 0;
      printf("包个数%d,CRC16=%d id=%d\n",info->transfer.packNum,info->transfer.crc16,info->transfer.id);
      dev_list_node_set(srcIP, info);
      dev_list_show_node(srcIP);
    }
    else //if(msg->streamAck.error == 2)//不传了
    {
      printf("传输设备不存在");
      info->flag.putDevConfig = 0;
      info->flag.routeGetConfig = 0;
      info->transfer.flag = TRANSFE_NONE;
      dev_list_node_set(srcIP, info);
      dev_list_show_node(srcIP);
    }
  }
  else
  {
    printf("srcIp =%d srcMac =%d\n",srcIP,srcMac);
    OS_ASSERT(NULL);
  }

  free(info);
  free(msg);
  return result;
}

static int transfer_req_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;
  int fromMac;
  int streamPos;
  int streamID;
  int selfIP; 

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  selfIP = get_self_nrf_srcaddr();//获取到自己的ip地址

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);

  srcIP = u8_to_u16(msg->transferReq.ipH,msg->transferReq.ipL);

  streamPos = u8_to_u16(msg->transferReq.posH,msg->transferReq.posL);//
  streamID = msg->transferReq.streamID;       //
  result = dev_list_node_get(srcIP, info);
  if(result == 0 && info->auth == Auth_Finsh)//能获取到这个节点
  {
    //if(info->flag.routeGetConfig == 0)//当前路由器上这个设备配置已经同步完成
    {
        if(streamID == info->ramConfig.id)//源数据和目标数据相同
        {
          unsigned char *data;

          if(streamPos*Tran_BUF_SIZE <= (info->ramConfig.size + Tran_BUF_SIZE))
          {
              //准备好数据发送
              data = info->ramConfig.data;
              data += streamPos*Tran_BUF_SIZE;
              memcpy(msg->transferAck.data,data,Tran_BUF_SIZE);//组织数据
              msg->transferAck.cmd = MSG_StreamData_ACK;
              msg->transferAck.ipH = (srcIP & 0xff00)>>8;//必须是源ip
              msg->transferAck.ipL = (srcIP & 0x00ff);
              msg->transferAck.result = 0;
              memcpy(msg->transferAck.data,data,Tran_BUF_SIZE);
              msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);
            }
            else//源数据有变动
            {
              printf("请求包位置异常info->transfer.curPos = %d  info->ramConfig.size=  %d\n",streamPos,info->ramConfig.size);
              msg->transferAck.cmd = MSG_StreamData_ACK;
              msg->transferAck.ipH = (srcIP & 0xff00)>>8;//必须是源ip
              msg->transferAck.ipL = (srcIP & 0x00ff);
              msg->transferAck.result = 1;
              msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);
            }
          }
          else
          {
            printf("上级设备配置的ID已经改变info->transfer.id = %d  == %d\n",info->ramConfig.id,streamID);
            msg->transferAck.cmd = MSG_StreamData_ACK;
            msg->transferAck.ipH = (srcIP & 0xff00)>>8;//必须是源ip
            msg->transferAck.ipL = (srcIP & 0x00ff);
            msg->transferAck.result = 1;
            msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);
          }
    }
    /*else//异常
    {
      msg->transferAck.cmd = MSG_StreamData_ACK;
      msg->transferAck.ipH = (srcIP & 0xff00)>>8;//必须是源ip
      msg->transferAck.ipL = (srcIP & 0x00ff);
      msg->transferAck.result = 2;
      msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);
      //OS_ASSERT(NULL);
    }*/
  }
  else//异常(有可能来自附近的网络干扰)
  {
    printf("none this device\n");
    msg->transferAck.cmd = MSG_StreamData_ACK;
    msg->transferAck.ipH = (srcIP & 0xff00)>>8;//必须是源ip
    msg->transferAck.ipL = (srcIP & 0x00ff);
    msg->transferAck.result = 3;
    msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);
    //OS_ASSERT(NULL);
  }

  msg->transferAck.cmd = MSG_StreamData_ACK;
  msg->transferAck.ipH = (selfIP & 0xff00)>>8;
  msg->transferAck.ipL = (selfIP & 0x00ff);

  free(info);
  free(recv);
  free(msg);

  return result;
}

static int transfer_ack_process(RxNrfMsgDef * raw)
{
  int result;
  int selfIP;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);
  selfIP = get_self_nrf_srcaddr();//获取到自己的ip地址
  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);
  srcIP = u8_to_u16(msg->transferAck.ipH,msg->transferAck.ipL);//源IP

  result = dev_list_node_get(srcIP,info);
  if(result == 0)//这个设备确实挂在这个路由器上
  {
    if(msg->transferAck.result == 0)//获取数据成功
    {
      memcpy(info->ramConfig.data+info->transfer.curPos*Tran_BUF_SIZE,msg->transferAck.data,Tran_BUF_SIZE);//保存数据
      printf("stream recv pos=%d\n",info->transfer.curPos);
      if(info->transfer.curPos >= (info->transfer.packNum-1))//传输完成
      {
        int crc16;

        crc16 = CRC16_2(info->transfer.packNum*Tran_BUF_SIZE,info->ramConfig.data);
        printf("%s\n",info->ramConfig.data);
        if(crc16 == info->transfer.crc16)
        {
          info->ramConfig.crc16 = crc16;
          info->ramConfig.id = info->transfer.id;
          info->ramConfig.size  = info->transfer.packNum*Tran_BUF_SIZE;
          //info->flag.putDevConfig = 1;//让下级路由器下拉配置
          //info->flag.routeGetConfig = 0;//非获取配置状态
          info->transfer.result = 1;//传输成功
          info->transfer.curPos = 0;
          dev_list_node_set(srcIP,info);
          dev_list_show_node(srcIP);
          printf("^_^ config file crc16 verify ok\n");
          info->transfer.flag = TRANSFE_FINSH_RX;
          memset(msg->buf,0,LAN_MSG_FIX_LEN);
          msg->REVTResultReq.cmd = MSG_REVTResult_Req;
          msg->REVTResultReq.evtType = 2;
          msg->REVTResultReq.result = 1;
          msg->REVTResultReq.ipH = (srcIP & 0xff00)>>8;
          msg->REVTResultReq.ipL = (srcIP & 0x00ff)>>0;
          msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//向上级路由器请求配置更新完成。
          dev_list_node_set(srcIP,info);
          dev_list_show_node(srcIP);
        }
        else//更新失败
        {
          //info->transfer.flag = TRANSFE_FINSH_RX;
          printf("T_T config file crc16 verify error!!!\n%s\n transfer.crc16 = %d ramConfig.crc16=%d crc16=%d\n",info->ramConfig.data,info->transfer.crc16,info->ramConfig.crc16,crc16);
          info->flag.putDevConfig = 0;//发送失败，不让下级设备拉配置
          info->flag.routeGetConfig = 0;//非获取配置状态
          info->transfer.curPos = 0;
           info->transfer.flag = TRANSFE_START_RX;// 重新请求标志
          dev_list_node_set(srcIP,info);
          dev_list_show_node(srcIP);
        }
      }
      else//还没有传输完成
      {
        info->transfer.curPos++;
        dev_list_node_set(srcIP,info);
      }
    }
    else if(msg->transferAck.result == 1) //上级设备的配置流ID更新
    {
        info->transfer.curPos = 0;
        info->transfer.flag = TRANSFE_START_RX;// 重新请求标志
        dev_list_node_set(srcIP,info);
        dev_list_show_node(srcIP);
    }
    else if(msg->transferAck.result == 2)//数据丢弃
    {
        printf("表示路由中没有这个流数据，丢弃!!!");
        info->flag.putDevConfig = 0;
        info->flag.routeGetConfig = 0;
        memset(msg->buf,0,LAN_MSG_FIX_LEN);
        msg->REVTResultReq.cmd = MSG_REVTResult_Req;
        msg->REVTResultReq.evtType = 2;
        msg->REVTResultReq.result = 0;
        msg->REVTResultReq.ipH = (srcIP & 0xff00)>>8;
        msg->REVTResultReq.ipL = (srcIP & 0x00ff)>>0;
        msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//向上级路由器请求配置更新完成。
        dev_list_node_set(srcIP,info);
        dev_list_show_node(srcIP);
    }
    else if(msg->transferAck.result == 3)//数据丢弃
    {
      printf("表示路由中没有这个设备,丢弃");
      info->flag.putDevConfig = 0;
      info->flag.routeGetConfig = 0;
      info->transfer.flag = TRANSFE_NONE;
      dev_list_node_set(srcIP,info);
      dev_list_show_node(srcIP);
    }
  }
  else
  {
    OS_ASSERT(NULL);
  }


  free(msg);
  free(recv);
  free(info);

  return result;
}

static int dev_auth_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  DevInfoDef_p selfInfo;//路由器自己的信息
  int fromIP;
  int fromMac;
  int selfIP;
  int dhcpDev;//设备获取的IP地址
  int reqType;
  int linkNum;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  selfInfo = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(selfInfo);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体

  fromIP = u8_to_u16(msg->devAuthReq.ipH,msg->devAuthReq.ipL);
  fromMac = u8_to_u32(msg->devAuthReq.macH1,msg->devAuthReq.macH0,msg->devAuthReq.macL1,msg->devAuthReq.macL0);
  reqType = msg->devAuthReq.type;//请求类型
  selfIP = get_self_nrf_srcaddr();
  dev_list_node_get(selfIP,selfInfo);


  //printf(">>>>>>>>>>>>>%d %d\n",selfIP,fromMac);
  if(RouteWorkMode == CCOL)//集中器收到直接应答鉴权
  {
    dhcpDev = dev_list_dhcp_get(fromMac,info);
    if(dhcpDev != -1)//获取到IP
    {
      linkNum = dev_list_num_get();//获取所以设备的数量
      msg->devAuthAck.cmd = MSG_Dev_Auth_Ack;
      msg->devAuthAck.dhcpH = (dhcpDev & 0xff00)>>8;
      msg->devAuthAck.dhcpL = (dhcpDev & 0x00ff);
      msg->devAuthAck.ipH = (selfIP & 0xff00) >> 8;
      msg->devAuthAck.ipL = (selfIP & 0x00ff);
      msg->devAuthAck.macH1 = (fromMac & 0xff000000)>>24;
      msg->devAuthAck.macH0 = (fromMac & 0x00ff0000)>>16;
      msg->devAuthAck.macL1 = (fromMac & 0x0000ff00)>>8;
      msg->devAuthAck.macL0 = (fromMac & 0x000000ff)>>0;
      msg->devAuthAck.depth = selfInfo->depthRoute;
      msg->devAuthAck.numH = (linkNum & 0xff00)>>8;
      msg->devAuthAck.numL = (linkNum & 0x00ff);
      if(info->auth == Auth_Finsh)
      {
        msg->devAuthAck.result = 3;
      }
      else
      {
          msg->devAuthAck.result = 1;
      }
      
      msg_send_dev(msg,recv,fromIP,0);//发送应答
      if(info->auth != Auth_Finsh && info->auth != Auth_No)//设备还没有鉴权就是还没上线过
      {
        if((reqType == 1))
        {
          info->auth = Auth_Finsh;
          info->route[info->depthRoute] = selfIP;
          info->route[info->depthRoute+1] = dhcpDev;
        }
        else if(reqType == 0)//只是查询鉴权
        {
          info->auth = Auth_Ask;
        }
      }
      else if(info->auth == Auth_Finsh)//设备已经鉴权则需要更新路由表
      {
          if((reqType == 1))
          {
            info->route[info->depthRoute+1] = dhcpDev;
            info->flag.putDevConfig = 1;//重新鉴权需要重置标志位
            info->flag.putDevPrice = 1;
          }
      }
      dev_list_node_set(dhcpDev,info);
    }
    else//获取IP失败,拒绝应答
    {
      printf("get dhcp fail %d\n",dhcpDev);
      msg->devAuthAck.cmd = MSG_Dev_Auth_Ack;
      msg->devAuthAck.dhcpH = (dhcpDev & 0xff00)>>8;
      msg->devAuthAck.dhcpL = (dhcpDev & 0x00ff);
      msg->devAuthAck.ipH = (selfIP & 0xff00) >> 8;
      msg->devAuthAck.ipL = (selfIP & 0x00ff);
      msg->devAuthAck.macH1 = (fromMac & 0xff000000)>>24;
      msg->devAuthAck.macH0 = (fromMac & 0x00ff0000)>>16;
      msg->devAuthAck.macL1 = (fromMac & 0x0000ff00)>>8;
      msg->devAuthAck.macL0 = (fromMac & 0x000000ff)>>0;
      msg->devAuthAck.depth = selfInfo->depthRoute;
      msg->devAuthAck.numH = (linkNum & 0xff00)>>8;
      msg->devAuthAck.numL = (linkNum & 0x00ff);
      msg->devAuthAck.result = 0;
      msg_send_dev(msg,recv,fromIP,0);//发送应答
    }
  }
  else//路由器
  {
    //判断这个设备是否为没有权限的设备
    result = unAuth_list_find_mac(fromMac);
    if(result != -1)//这个设备是没有权限的设备
    {
      msg->devAuthAck.cmd = MSG_Dev_Auth_Ack;
      msg->devAuthAck.dhcpH = (dhcpDev & 0xff00)>>8;
      msg->devAuthAck.dhcpL = (dhcpDev & 0x00ff);
      msg->devAuthAck.ipH = (selfIP & 0xff00) >> 8;
      msg->devAuthAck.ipL = (selfIP & 0x00ff);
      msg->devAuthAck.macH1 = (fromMac & 0xff000000)>>24;
      msg->devAuthAck.macH0 = (fromMac & 0x00ff0000)>>16;
      msg->devAuthAck.macL1 = (fromMac & 0x0000ff00)>>8;
      msg->devAuthAck.macL0 = (fromMac & 0x000000ff)>>0;
      msg->devAuthAck.depth = selfInfo->depthRoute;
      msg->devAuthAck.numH = (linkNum & 0xff00)>>8;
      msg->devAuthAck.numL = (linkNum & 0x00ff);
      msg->devAuthAck.result = 0;
      printf("This %d Mac None Auth!!!!\n",fromMac);
      msg_send_dev(msg,recv,fromIP,0);
    }
    else//这个设备还没有获取到信息
    {
      dhcpDev = dev_list_dhcp_get(fromMac,info);
      if(dhcpDev != -1)//路由上已经有这个设备的信息
      {
        linkNum = dev_list_num_get();//获取所以设备的数量
        msg->devAuthAck.cmd = MSG_Dev_Auth_Ack;
        msg->devAuthAck.dhcpH = (dhcpDev & 0xff00)>>8;
        msg->devAuthAck.dhcpL = (dhcpDev & 0x00ff);
        msg->devAuthAck.ipH = (selfIP & 0xff00) >> 8;
        msg->devAuthAck.ipL = (selfIP & 0x00ff);
        msg->devAuthAck.macH1 = (fromMac & 0xff000000)>>24;
        msg->devAuthAck.macH0 = (fromMac & 0x00ff0000)>>16;
        msg->devAuthAck.macL1 = (fromMac & 0x0000ff00)>>8;
        msg->devAuthAck.macL0 = (fromMac & 0x000000ff)>>0;
        msg->devAuthAck.depth = selfInfo->depthRoute;
        msg->devAuthAck.numH = (linkNum & 0xff00)>>8;
        msg->devAuthAck.numL = (linkNum & 0x00ff);
        msg->devAuthAck.result = 1;
        msg_send_dev(msg,recv,fromIP,0);//发送应答让设备等等
        if(reqType == 1)//鉴权
        {
            info->auth = Auth_Finsh;
            info->flag.putDevConfig = 1;
            info->flag.putDevPrice = 1;
            info->depthRoute = selfInfo->depthRoute;
            info->route[info->depthRoute] = selfIP;
            info->route[info->depthRoute+1] = dhcpDev;
        }
        else//查询鉴权
        {
            info->auth = Auth_Ask;
        }

        dev_list_node_set(dhcpDev,info);
      }
      else//路由器上没有
      {
        printf("Route None This Mac %d\n",fromMac);
        dev_list_show();
        msg->devAuthAck.cmd = MSG_Dev_Auth_Ack;
        msg->devAuthAck.dhcpH = (dhcpDev & 0xff00)>>8;
        msg->devAuthAck.dhcpL = (dhcpDev & 0x00ff);
        msg->devAuthAck.ipH = (selfIP & 0xff00) >> 8;
        msg->devAuthAck.ipL = (selfIP & 0x00ff);
        msg->devAuthAck.macH1 = (fromMac & 0xff000000)>>24;
        msg->devAuthAck.macH0 = (fromMac & 0x00ff0000)>>16;
        msg->devAuthAck.macL1 = (fromMac & 0x0000ff00)>>8;
        msg->devAuthAck.macL0 = (fromMac & 0x000000ff)>>0;
        msg->devAuthAck.depth = selfInfo->depthRoute;
        msg->devAuthAck.numH = (linkNum & 0xff00)>>8;
        msg->devAuthAck.numL = (linkNum & 0x00ff);
        msg->devAuthAck.result = 2;//等待
        msg_send_dev(msg,recv,fromIP,0);//发送应答         

        //发送获取一个设备的ip
        NrfMsgDef_p msg,recv;

        msg = calloc(1,sizeof(MsgSendMailDef));
        OS_ASSERT(msg);

        recv = calloc(1,sizeof(MsgSendMailDef));
        OS_ASSERT(recv)

        memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝接收的原始数据到报文结构体

        msg->routeAuthReq.cmd = MSG_AP_Auth_Req;//0x01
        msg->routeAuthReq.ch = msg->devAuthReq.ch;
        msg->routeAuthReq.macH1 = (fromMac & 0xff000000)>>24;
        msg->routeAuthReq.macH0 = (fromMac & 0x00ff0000)>>16;
        msg->routeAuthReq.macL1 = (fromMac & 0x0000ff00)>>8;
        msg->routeAuthReq.macL0 = (fromMac & 0x000000ff)>>0;
        msg->routeAuthReq.type = reqType;//价签的鉴权类型
        msg->routeAuthReq.ipH = (selfIP & 0xff00) >> 8;
        msg->routeAuthReq.ipL = (selfIP & 0x00ff);
        result = msg_route_send(msg,recv,-1,0);//向上级路由鉴权

      }

    }
  }

  free(msg);
  free(recv);
  free(info);
  free(selfInfo);

  return result;
}


int dev_req_evt_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int fromIP;
  int fromMac;
  int selfIP;
  int dhcpDev;
  devEvtDef event = {0};
  unsigned char  price[4];

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();

  fromIP = u8_to_u16(msg->devEvtReq.ipH,msg->devEvtReq.ipL);
  fromMac = u8_to_u32(msg->devEvtReq.macH1,msg->devEvtReq.macH0,msg->devEvtReq.macL1,msg->devEvtReq.macL0);

  dhcpDev = dev_list_dhcp_get(fromMac,info);
  if(dhcpDev != -1 && info->auth == Auth_Finsh)//设备存在
  {
    int isAck = 0;
    unsigned int eventTmp;

    eventTmp = event.eventU32;

    float_to_u8(price,info->price.data);
    if(info->flag.putDevConfig)//需要更新配置
    {
      // info->flag.putDevConfig = 0;//设备价格更新了
      event.eventChar.UpdateConfig = 1;
      isAck = 1;
    }
    if(info->flag.putDevPrice)//更新价格
    {
      //info->flag.putDevPrice = 0;
      event.eventChar.UpdatePrice = 1;
      isAck = 1;
    }
    printf("isAck = %d\n",isAck);
    if(isAck)
    {
      memset(msg->buf,0,LAN_MSG_FIX_LEN);
      msg->devEvtAck.cmd = MSG_DevEvt_Ack;
      msg->devEvtAck.ipH = (selfIP & 0xff00)>>8;
      msg->devEvtAck.ipL = (selfIP & 0x00ff);
      msg->devEvtAck.eventH1 = (event.eventU32 & 0xff000000)>>24;
      msg->devEvtAck.eventH0 = (event.eventU32 & 0x00ff0000)>>16;
      msg->devEvtAck.eventL1 = (event.eventU32 & 0x0000ff00)>>8;
      msg->devEvtAck.eventL0 = (event.eventU32 & 0x000000ff)>>0;
      msg->devEvtAck.priceH1 = price[0];
      msg->devEvtAck.priceH0 = price[1];
      msg->devEvtAck.priceL1 = price[2];
      msg->devEvtAck.priceL0 = price[3];
      msg_send_dev(msg,recv, fromIP,0);
    }
  }
  else                   //没有这个设备
  {
    printf("This Device None dhcp!!!!\n");
    memset(msg->buf,0,LAN_MSG_FIX_LEN);

    event.eventChar.DevReAuth = 1;
    msg->devEvtAck.cmd = MSG_DevEvt_Ack;
    msg->devEvtAck.ipH = (selfIP & 0xff00)>>8;
    msg->devEvtAck.ipL = (selfIP & 0x00ff);
    msg->devEvtAck.eventH1 = (event.eventU32 & 0xff000000)>>24;
    msg->devEvtAck.eventH0 = (event.eventU32 & 0x00ff0000)>>16;
    msg->devEvtAck.eventL1 = (event.eventU32 & 0x0000ff00)>>8;
    msg->devEvtAck.eventL0 = (event.eventU32 & 0x000000ff)>>0;
    msg->devEvtAck.priceH1 = ((unsigned int)price & 0xff000000)>>24;
    msg->devEvtAck.priceH0 = ((unsigned int)price & 0xff000000)>>16;
    msg->devEvtAck.priceL1 = ((unsigned int)price & 0xff000000)>>8;
    msg->devEvtAck.priceL0 = ((unsigned int)price & 0xff000000)>>0;
    msg_send_dev(msg,recv, fromIP,0);
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}


int route_evt_req_process(RxNrfMsgDef *raw)
{
  /*
     只有下一级路由才能收到
     */
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int fromIP;
  int fromMac;
  int selfIP;
  int devIP;
  int dhcpDev;
  unsigned char price[4];
  routeEvtDef event = {0};

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();

  //解析报文
  fromIP = u8_to_u16(msg->routeEvtReq.ipH,msg->routeEvtReq.ipL);//直接通信的设备ip
  devIP = u8_to_u16(msg->routeEvtReq.devIPH,msg->routeEvtReq.devIPL);//要通知的设备ip
  event.eventU32 = u8_to_u32(msg->routeEvtReq.eventH1,msg->routeEvtReq.eventH0,msg->routeEvtReq.eventL1,msg->routeEvtReq.eventL0);//路由的事件

  result = dev_list_node_get(devIP,info);
  if(result  == 0)//有这个设备
  {
    dev_list_show();
    if(event.eventBit.UpdateConfig)//需要下拉配置文件
    {
      if(info->ramConfig.id  != msg->routeEvtReq.streamID)//上级路由要求下发的流数据和现在设备以存在流数据相不同
      {
        printf("route device mac: %d need get config file\n",info->mac);
        if(info->transfer.flag == TRANSFE_NONE)//如果这个设备还么有进行流数据传输
        {
          info->transfer.flag  = TRANSFE_START_RX;//收到流数据下拉请求，设备要进入流数据请求的接收端
          info->flag.routeGetConfig = 1;//并且当前路由器进入正在下拉配置的状态
        }
      }
      else
      {
          printf("要求下发的流数据与现在设备已存在流数据相同!!!!!\n");
      }
    }
    if(event.eventBit.UpdatePrice)//表示需要下拉价格
    {
      if(info->price.buf[0] == msg->routeEvtReq.priceH1
          && info->price.buf[1] == msg->routeEvtReq.priceH0
          && info->price.buf[2] == msg->routeEvtReq.priceL1
          && info->price.buf[3] == msg->routeEvtReq.priceL0)//如果下发的价格与当前价格一致则不进行更新
      {
          //价格没有改变是04应答发送失败造成的
      }
      else
      {
          info->flag.putDevPrice = 1;//当前这级别路由器需要下发价格了
          info->price.buf[0] = msg->routeEvtReq.priceH1;
          info->price.buf[1] = msg->routeEvtReq.priceH0;
          info->price.buf[2] = msg->routeEvtReq.priceL1;
          info->price.buf[3] = msg->routeEvtReq.priceL0;
      }

      printf("need get price %f\n",info->price.data);
    }
    
    dev_list_node_set(devIP, info);//收到就保存事件，
    
    //事件已经收到，应答服务器
    msg->routeEvtAck.cmd = MSG_RouteEvt_Ack;
    msg->routeEvtAck.ipH = (selfIP & 0xff00)>>8;
    msg->routeEvtAck.ipL = (selfIP & 0x00ff)>>0;
    msg->routeEvtAck.eventH1 = (event.eventU32 & 0xff000000)>>24;
    msg->routeEvtAck.eventH0 = (event.eventU32 & 0x00ff0000)>>16;
    msg->routeEvtAck.eventL1 = (event.eventU32 & 0x0000ff00)>>8;
    msg->routeEvtAck.eventL0 = (event.eventU32 & 0x000000ff)>>0;
    msg->routeEvtAck.devIPH = (devIP & 0xff00)>>8;
    msg->routeEvtAck.devIPL = (devIP & 0x00ff);
    result = msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//给上级设备应答
    if(result != 0)//事件应答成功
    {
      result = msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);
      if(result != 0)//事件应答成功
      {
        printf("MSG_RouteEvt_Ack fail !!!\n");
      }
    }

    dev_list_show_node(devIP);
  }
  else//没有这个设备
  {
    /*
        设备没有鉴权暂时先清除事件
    */
    printf("route in %d none auth !!!!!!\n,",devIP);
    msg->routeEvtAck.cmd = MSG_RouteEvt_Ack;
    msg->routeEvtAck.ipH = (selfIP & 0xff00)>>8;
    msg->routeEvtAck.ipL = (selfIP & 0x00ff)>>0;
    msg->routeEvtAck.eventH1 = (event.eventU32 & 0xff000000)>>24;
    msg->routeEvtAck.eventH0 = (event.eventU32 & 0x00ff0000)>>16;
    msg->routeEvtAck.eventL1 = (event.eventU32 & 0x0000ff00)>>8;
    msg->routeEvtAck.eventL0 = (event.eventU32 & 0x000000ff)>>0;
    msg->routeEvtAck.devIPH = msg->routeEvtReq.devIPH;
    msg->routeEvtAck.devIPL = msg->routeEvtReq.devIPL;
    result = msg_send_dev(msg,recv,fromIP,0);
    if(result != 0)
    {
        printf("MSG_RouteEvt_Ack Fail\n");
    }
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}

int route_evt_ack_process(RxNrfMsgDef *raw)
{
  /*
     只有上级设备才会收到这条报文
     */
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int fromIP;
  int devIP;
  int fromMac;
  int selfIP;
  routeEvtDef event;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();

  fromIP = u8_to_u16(msg->routeEvtAck.ipH,msg->routeEvtAck.ipL);
  devIP = u8_to_u16(msg->routeEvtAck.devIPH,msg->routeEvtAck.devIPL);
  event.eventU32 = u8_to_u32(msg->routeEvtAck.eventH1,msg->routeEvtAck.eventH0,msg->routeEvtAck.eventL1,msg->routeEvtAck.eventL0);

  result = dev_list_node_get(devIP,info);
  if(result != -1)//获取设备节点
  {
    if(event.eventU32 != 0)//有事件下发成功
    {
      if(event.eventBit.UpdateConfig && info->transfer.flag == TRANSFE_NONE)//更新配置文件当前设备一定没有处于传输过程中才可重新开始
      {
        printf("put ip:%d device config ok\n",devIP);
        info->flag.putDevConfig = 0;//
        //info->flag.routeGetConfig = 1;//
        //info->transfer.flag = TRANSFE_START_TX;
      }
      if(event.eventBit.UpdatePrice)//更新价格
      {
        printf("put ip:%d deveice price ok\n",devIP);
        info->flag.putDevPrice = 0;//
      }
      dev_list_node_set(devIP,info);
      dev_list_show_node(devIP);
    }
    else//事件下发失败，重新下发
    {
      //info->flag.putDevPrice = 0;
      //dev_list_node_set(devIP,info);
      //dev_list_show_node(devIP);
    }
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}

int dev_updateOk_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;//报文发起者ip
  int dstIP;//本次处理报文应答的目标地址，
  int fromMac;
  int selfIP;
  unsigned char evtType;
  //routeEvtDef event;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);
  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();

  srcIP = u8_to_u16(msg->UpdateOkReq.ipH,msg->UpdateOkReq.ipL);
  evtType = msg->UpdateOkReq.evtType;

  result = dev_list_node_get(srcIP,info);
  if(result == 0 && info->auth == Auth_Finsh)//路由器上有这个设备
  {
    //清除更新标志
    //应答下级设备
    //通知上级路由
    result = 1;
    switch(evtType)
    {
      case 1://表示路由器价格更新成
        {
          break;
        }
      case 2://表示路由器更新配置成功
        {
          //info->transfer.flag = 0;
          break;
        }
      case 3://表示价签设备更新价格成功
        {
          info->flag.putDevPrice  = 0;
          info->flag.DevPriceOk = 1;    //下级设备更新完成。
          break;
        }
      case 4://表示价签设备更新配置成功
        {
          info->flag.putDevConfig= 0;
          info->flag.DevConfigOk = 1;
          break;
        }
       case 5:
       {
            if(RouteWorkMode != CCOL)//不是集中器
            {
              info->flag.devAuthFinsh = 1;//标记设备鉴权成功
            }
            
            break;
       }
      default:
        {
          msg->UpdateOkReq.evtType = 0;
          result = 0;
          break;
        }
    }
    dev_list_node_set(srcIP, info);//保存设备信息
    dev_list_show_node(srcIP);

    memset(msg,0,sizeof(NrfMsgDef));
    msg->UpdateOkAck.cmd = MSG_UpdateOk_Ack;
    msg->UpdateOkAck.ipH  =  (srcIP & 0xff00)>>8;
    msg->UpdateOkAck.ipL = (srcIP & 0x00ff)>>0;
    msg->UpdateOkAck.result = result;
    msg->UpdateOkAck.evtType = evtType;
    //msg_route_send(msg, recv, 1, 0);
    dstIP = info->route[info->depthRoute+1];//下级ip
    if(dstIP  == 0)//路由器没有这个设备的下级路由信息
    {
        dstIP =  srcIP;
    }
    msg_send_dev(msg, recv, dstIP, 0);
  }
  else//这个设备不存在,路由器重启导致
  {
    memset(msg,0,sizeof(NrfMsgDef));
    msg->UpdateOkAck.cmd = MSG_UpdateOk_Ack;
    msg->UpdateOkAck.ipH  =  (selfIP & 0xff00)>>8;
    msg->UpdateOkAck.ipL = (selfIP & 0x00ff)>>0;
    msg->UpdateOkAck.result = 2;//表示路由器上没有这个设备
    msg_send_dev(msg, recv, srcIP, 0);
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}

int dev_updateOk_ack_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;
  int dstIP;
  int selfIP;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);
  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();
  srcIP = u8_to_u16(msg->UpdateOkAck.ipH,msg->UpdateOkAck.ipL);

  result = dev_list_node_get(srcIP, info);
  if(result == 0)
  {
    if(msg->UpdateOkAck.result == 1)//上级路由器处理这个应答时成功的
    {
      switch(msg->UpdateOkAck.evtType)
      {
        case 1:
          {
            break;
          }
        case 2://表示这个设备的配置信息以下拉成功
          {
            info->transfer.flag = 0;
            info->flag.syncFinishFlag = 1;
            info->flag.putDevConfig = 1;
            info->transfer.flag = TRANSFE_NONE;
            break;
          }
        case 3://表示当前这一级别路由器更新价格成功
          {
            info->flag.DevPriceOk = 0;
            break;
          }
        case 4://表示当前这一级别路由器更新配置成功
          {
            info->flag.DevConfigOk = 0;
            break;
          }
        default:
          {
            break;
          }
      }
      dev_list_node_set(srcIP, info);
      dev_list_show_node(srcIP);
    }
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}


static int route_evt_result_req_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;//报文发起者ip
  int dstIP;//本次处理报文应答的目标地址，
  int fromMac;
  int selfIP;
  int uponIP;//下级IP
  unsigned char evtType;
  unsigned char evtRsult;
  //routeEvtDef event;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();

  srcIP = u8_to_u16(msg->REVTResultReq.ipH,msg->REVTResultReq.ipL);
  uponIP = u8_to_u16(msg->REVTResultReq.uponIPH,msg->REVTResultReq.uponIPL);
  evtType = msg->REVTResultReq.evtType;
  evtRsult = msg->REVTResultReq.result;

  result = dev_list_node_get(srcIP,info);
  if(result == 0)//路由器上有这个设备
  {
    //清除更新标志
    //应答下级设备
    //通知上级路由
    result = 1;
    switch(evtType)
    {
      case 1://表示路由器价格更新成
        {
          break;
        }
      case 2://表示路由器更新配置成功
        {
          if(evtRsult == 1)//如果下级设备下拉配置成功
          {
            printf("传输完成^_^\n");
            info->flag.DevConfigOk = 0;
            info->flag.routeGetConfig = 0;
          }
          info->transfer.flag = TRANSFE_NONE;//清除上级设备的传输标志

          break;
        }
      case 3://表示价签设备更新价格成功
        {
          if(evtRsult == 1)
          {
            info->flag.putDevPrice  = 0;
            info->flag.DevPriceOk = 1;    //下级设备更新完成。                   
            dev_list_node_set(srcIP, info);//保存设备信息
            dev_list_show_node(srcIP);
          }


          break;
        }
      case 4://表示价签设备更新配置成功
        {
          if(evtRsult == 1)//下级设备处理成功
          {
            info->flag.DevConfigOk = 1;
            dev_list_node_set(srcIP, info);//保存设备信息
          }

          break;
        }
      case 5:
      {
          if(RouteWorkMode == ROUTE)//只有路由器才需要上报
          {
            info->flag.devAuthFinsh = 1;
          }
          info->route[info->depthRoute] = selfIP;
          info->route[info->depthRoute+1] = uponIP;
          info->auth = Auth_Finsh;//鉴权完成
          dev_list_node_set(srcIP, info);//上报只要收到就马上执行
         break;
      }
      default:
        {
          msg->UpdateOkReq.evtType = 0;
          result = 0;
          break;
        }
    }
    memset(msg,0,sizeof(NrfMsgDef));
    msg->REVTResultAck.cmd = MSG_REVTResult_Ack;
    msg->REVTResultAck.ipH  =  (srcIP & 0xff00)>>8;
    msg->REVTResultAck.ipL = (srcIP & 0x00ff)>>0;
    msg->REVTResultAck.result = result;
    msg->REVTResultAck.evtType = evtType;
    result = msg_send_dev(msg, recv, info->route[info->depthRoute+1], 0);//应答下级设备
    if(result == 0)//下发成功
    {
      dev_list_node_set(srcIP, info);//保存设备信息
      dev_list_show_node(srcIP);
    }
    else//下发应答失败
    {
       printf("MSG_REVTResult_Ack send fail !!!\n");
    }
  }
  else//没有这个设备
  {
    memset(msg,0,sizeof(NrfMsgDef));
    msg->REVTResultAck.cmd = MSG_REVTResult_Ack;
    msg->REVTResultAck.ipH  =  (selfIP & 0xff00)>>8;
    msg->REVTResultAck.ipL = (selfIP & 0x00ff)>>0;
    msg->REVTResultAck.result = 2;//表示路由器上没有这个设备

    dstIP = info->route[info->depthRoute+1];
    result = msg_send_dev(msg, recv, dstIP, 0);
    if(result == 0)//发送成功
    {

    }
    else
    {

    }
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}

static int route_evt_result_ack_process(RxNrfMsgDef *raw)
{
  int result;
  NrfMsgDef_p  msg;
  NrfMsgDef_p  recv;
  DevInfoDef_p info;
  int srcIP;
  int dstIP;
  int selfIP;

  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);
  memcpy(msg->buf,raw->buf,LAN_MSG_FIX_LEN);//拷贝原始数据到报文结构体
  selfIP = get_self_nrf_srcaddr();
  srcIP = u8_to_u16(msg->REVTResultAck.ipH,msg->REVTResultAck.ipL);

  result = dev_list_node_get(srcIP, info);
  if(result == 0)
  {
    if(msg->REVTResultAck.result == 1)//上级路由器处理这个应答时成功的
    {
      switch(msg->REVTResultAck.evtType)
      {
        case 1://表示这个设备价格下拉成功
          {
            info->flag.DevPriceOk = 0;//更新完成清零
            break;
          }
        case 2://表示这个设备的配置信息以下拉成功
          {
            info->transfer.flag = 0;
            info->flag.routeGetConfig = 0;
            info->flag.putDevConfig = 1;
            break;
          }
        case 3://表示这个设备更新价格成功
          {
            info->flag.DevPriceOk = 0;
            break;
          }
        case 4://表示这个设备更新配置文件成功
          {
            info->flag.DevConfigOk = 0;
            break;
          }
        case 5:
        {
            info->flag.devAuthFinsh = 0;
            break;
        }
        default:
          {
            break;
          }
      }
      dev_list_node_set(srcIP, info);
      dev_list_show_node(srcIP);
    }
  }

  free(msg);
  free(recv);
  free(info);

  return result;
}


static int route_nrf_msg_process(RxNrfMsgDef *msg)
{
  switch(msg->buf[MSG_CMD])
  {
    case MSG_AP_Auth_Req://接收到来自WAP的鉴权报文,应答这个报文
    {
      route_auth_req_process(msg);
      break;
    }
    case MSG_AP_Auth_Ack://收到AP鉴权应答
    {
        route_auth_ack_process(msg);
        break;
    }
    case MSG_RouteEvt_Req://收到路由器事件请求
      {
        //只有路由器能收到
        route_evt_req_process(msg);
        break;
      }
    case MSG_RouteEvt_Ack://上级设备才会收到
      {
        route_evt_ack_process(msg);
        break;
      }
    case MSG_Stream_Req://数据流请求
      {
        //集中器和路由器都有可能收到
        stream_req_process(msg);
        break;
      }
    case MSG_Stream_Ack://只有下级设备才会收到
      {
        stream_ack_process(msg);
        break;
      }
    case MSG_StreamData_Req://获取数据请求
      {
        //集中器和路由器都有可能收到
        transfer_req_process(msg);
        break;
      }
    case MSG_StreamData_ACK://获取数据应答保存数据
      {
        transfer_ack_process(msg);
        break;
      }
    case MSG_Dev_Auth_Req: //设备鉴权
      {
        dev_auth_process(msg);
        break;
      }
    case MSG_DevEvt_Req://设备查询
      {
        dev_req_evt_process(msg);
        break;
      }
    case MSG_UpdateOk_Req://更新成功请求
      {
        dev_updateOk_process(msg);
        break;
      }
    case MSG_UpdateOk_Ack://更新成功应答,路由器不可能收到
      {
        exit(0);
        //dev_updateOk_ack_process(msg);
        break;
      }
    case MSG_REVTResult_Req://路由器事件结果请求
      {
        route_evt_result_req_process(msg);
        break;
      }
    case MSG_REVTResult_Ack://路由器事件结构应答
      {
        route_evt_result_ack_process(msg);
        break;
      }
    default:
      {
        break;
      }
  }
  nrf_16byte_mode();
}

int msg_send_dev(NrfMsgDef *msg,NrfMsgDef *recv,int dst,int wait)
{
  NrfDevDef *nrf_dev;
  int result;
  int selfIP = 0;
  DevInfoDef_p devInfo;

  OS_ASSERT(msg);
  OS_ASSERT(recv);

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);
  devInfo = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(devInfo);

  //设置报文
  get_nrf_ap_config(nrf_dev);

  //设备地址
  set_nrfdev_dstaddr(nrf_dev,dst);//根据路由表发送到目标地址去

  nrf_dev->cmd = NRF_SEND_RECV;//发送接收模式
  nrf_dev->wait_time = wait;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;


  //获取发送与接收数据缓存资源
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  memcpy(nrf_dev->send_buf,&msg->buf,MAX_MSG_SIZE);
  //发送邮件
  nrf_send_mail(nrf_dev);

  if(nrf_dev->recv_buf[MSG_CMD] == NRF_SEND_ERROR)
  {
    result = -1;
  }
  else
  {
    result = 0;
    //memcpy(msg->buf,nrf_dev->recv_buf,NRF_16B_SIZE_TRX);
    memcpy(recv->buf,nrf_dev->recv_buf,NRF_16B_SIZE_TRX);
  }


  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);
  free(devInfo);

  return result;
}


//采用路由的方式发送报文1表示向下级发送，
//-1表示想上级发送
static int msg_route_send(NrfMsgDef *msg,NrfMsgDef *recv,int direction,int wait)
{
  NrfDevDef *nrf_dev;
  int result;
  int selfIP = 0;
  DevInfoDef_p devInfo;

  OS_ASSERT(msg);
  OS_ASSERT(recv);

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);
  devInfo = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(devInfo);

  //设置报文
  get_nrf_ap_config(nrf_dev);

  //上级路由
  selfIP = get_nrfdev_srcaddr(nrf_dev);//获取自身的ip
  dev_list_node_get(selfIP,devInfo);//取出自己的设备信息

  set_nrfdev_dstaddr(nrf_dev,devInfo->route[devInfo->depthRoute+direction]);//根据路由表发送到目标地址去
  nrf_dev->cmd = NRF_SEND_RECV;//发送接收模式
  nrf_dev->wait_time = wait;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;


  //获取发送与接收数据缓存资源
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  memcpy(nrf_dev->send_buf,&msg->buf,MAX_MSG_SIZE);
  //发送邮件
  nrf_send_mail(nrf_dev);

  if(nrf_dev->recv_buf[MSG_CMD] == NRF_SEND_ERROR)
  {
    result = -1;
  }
  else
  {
    result = 0;
    //memcpy(msg->buf,nrf_dev->recv_buf,NRF_16B_SIZE_TRX);
    memcpy(recv->buf,nrf_dev->recv_buf,NRF_16B_SIZE_TRX);
  }


  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);
  free(devInfo);

  return result;
}


//发送路由鉴权请求
static int ap_msg_auth_req(int dstaddr,int ch,int srcaddr ,NrfMsgDef *msg)
{
  NrfDevDef *nrf_dev;
  int result;

  //获取报文数据资源
  nrf_dev = (NrfDevDef*)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);

  //设置报文
  get_nrf_ap_config(nrf_dev);
  set_nrfdev_srcaddr(nrf_dev,srcaddr);
  set_nrfdev_dstaddr(nrf_dev,dstaddr);
  nrf_dev->cmd = NRF_SEND_RECV;//发送接收模式
  nrf_dev->ch = ch;
  nrf_dev->wait_time = 20;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;


  //获取发送与接收数据缓存资源
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  memcpy(nrf_dev->send_buf,&msg->buf,MAX_MSG_SIZE);
  nrf_dev->send_buf[MSG_CMD] = MSG_AP_Auth_Req;
  //发送邮件
  nrf_send_mail(nrf_dev);

  if(nrf_dev->recv_buf[MSG_CMD] == NRF_SEND_ERROR)
  {
    result = -1;
  }
  else
  {
    result = 0;
    memcpy(msg->buf,nrf_dev->recv_buf,NRF_16B_SIZE_TRX);
  }


  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);

  return result;
}

static int nrf_16byte_mode(void)
{
  NrfDevDef *nrf_dev;
  int result;

  nrf_dev = (NrfDevDef *)calloc(1,sizeof(NrfDevDef));
  OS_ASSERT(nrf_dev);

  //设置nrf 收发器
  get_nrf_ap_config(nrf_dev);
  nrf_dev->cmd =  NRF_16B_MODE;
  nrf_dev->wait_time = 0;
  nrf_dev->data_len = NRF_16B_SIZE_TRX;


  //设置收发器数据缓存
  nrf_dev->send_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->send_buf);

  nrf_dev->recv_buf = calloc(1,nrf_dev->data_len);
  OS_ASSERT(nrf_dev->recv_buf);

  nrf_send_mail(nrf_dev);

  free(nrf_dev->send_buf);
  free(nrf_dev->recv_buf);
  free(nrf_dev);

  return result;
}

static int scan_wap_auth_process(void)
{
  //以0地址发送鉴权探针报文
  int ch;
  int RouteID;
  int i;
  ScanResultDef scanResult[MAX_ROUTE_NUM] = {0};
  int scanNum = 0;
  int authAP;
  int authCh;
  NrfMsgDef *authMsg;//确认鉴权
  int result;
  int outtime;
  int waitAuthFlag = 0;
  NrfMsgDef *msg;
  unsigned char sureChFlag = 0;
  
  msg = calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg)

  for(ch = 126;ch>=2;ch-=2)//通道
  {
    if(sureChFlag == 1)
    {
        break;
    }

    if((waitAuthFlag <= 5) && (waitAuthFlag >= 1)) //如果上一个通道有等待的
    {
        ch+=2;
        printf("ch = %d waiAuthFlag=%d\n",ch,waitAuthFlag);
    }
    else
    {
        waitAuthFlag = 0;
    }
    
    for(i=1;i<MAX_ROUTE_NUM;i++)
    {
      memset(msg->buf,0,LAN_MSG_FIX_LEN);
      //发送鉴权请求
      msg->routeAuthReq.ipH = MAX_ROUTE_NUM / 0xff;
      msg->routeAuthReq.ipL = MAX_ROUTE_NUM % 0xff;
      msg->routeAuthReq.ch = ch;
      msg->routeAuthReq.macH1 = (DevMac&0xff000000)>>24;
      msg->routeAuthReq.macH0 = (DevMac&0x00ff0000)>>16;
      msg->routeAuthReq.macL1 = (DevMac&0x0000ff00)>>8;
      msg->routeAuthReq.macL0 = (DevMac&0x000000ff)>>0;
      msg->routeAuthReq.type = 0;
      result = ap_msg_auth_req(i,ch,MAX_ROUTE_NUM,msg);
      if(result == 0)
      {            

        if(msg->routeAuthAck.cmd != MSG_AP_Auth_Ack)//收到其他类型的报文
        {
            i--;
            continue;
        }
        //解析鉴权应答报文
        if(msg->routeAuthAck.cmd == MSG_AP_Auth_Ack)
        {
          switch(msg->routeAuthAck.auth)//鉴权结果
          {
            case 0://拒绝链接
              {

                break;
              }
            case 3://表示已经鉴权
            case 1://表示允许连接，其他通道可以不用扫了.
              {
                if(scanNum < MAX_ROUTE_NUM)
                {
                  #ifdef Only_Route_Depth
                  if(msg->routeAuthAck.depthRoute == Only_Route_Depth)//指定连一个路由深度
                  #endif
                  {
                    scanResult[scanNum].routeIP = i;//ip上级路由的ip
                    scanResult[scanNum].ch = ch;//通信信道
                    scanResult[scanNum].tree = msg->routeAuthAck.depthRoute;//路由深度
                    scanResult[scanNum].dhcp = u8_to_u16(msg->routeAuthAck.dhcpH,msg->routeAuthAck.dhcpL);//自身获取的ip
                    scanResult[scanNum].num = u8_to_u16(msg->routeAuthAck.devNUmH,msg->routeAuthAck.devNumL);
                    //scanResult[scanNum].sum = u8_to_u16(msg->routeAuthAck.sumH, msg->routeAuthAck.sumL);
                    scanNum++;
                    sureChFlag = 1;
                  }
                }

                break;
              }
            case 2://表示正在获取权限信息
              {
                waitAuthFlag++;
                break;
              }
          }
        }
        //nrf_16byte_mode();
      }
    }
  }
  free(msg);
  
  nrf_16byte_mode();
  if(scanNum != 0)
  {
    NrfDevDef nrfconfig;

    printf("*_* Scan Route Num =%d\n",scanNum);
    //找到最小的路由等级
    int minTree = scanResult[0].tree;
    int minTreePos = 0;//深度最小的位置

    for(i=0;i<scanNum;i++)
    {
      if(scanResult[i].tree < minTree)
      {
        minTree = scanResult[i].tree;
        minTreePos = i;
      }
    }

    printf("Auth.ch = %d\n",scanResult[minTreePos].ch);
    printf("Auth.ip = %d\n",scanResult[minTreePos].routeIP);
    printf("Dev.dhcp = %d\n",scanResult[minTreePos].dhcp);
    printf("Dev.num = %d\n",scanResult[minTreePos].num);
    
    //设置nrfconfig
    get_nrf_ap_config(&nrfconfig);
    nrfconfig.ch = scanResult[minTreePos].ch;
    set_nrfdev_srcaddr(&nrfconfig,scanResult[minTreePos].dhcp);//设置自身地址
    set_nrfdev_dstaddr(&nrfconfig,scanResult[minTreePos].routeIP);
    set_nrf_ap_config(&nrfconfig);

    //确认鉴权     
    authMsg = calloc(1,sizeof(NrfMsgDef));
    OS_ASSERT(authMsg);

    authMsg->routeAuthReq.ipH = nrfconfig.src_addr[NRF_ADDR_LEN-2];
    authMsg->routeAuthReq.ipL = nrfconfig.src_addr[NRF_ADDR_LEN-1];
    authMsg->routeAuthReq.ch = nrfconfig.ch;
    
    authMsg->routeAuthReq.macH1 = (DevMac&0xff000000)>>24;
    authMsg->routeAuthReq.macH0 = (DevMac&0x00ff0000)>>16;
    authMsg->routeAuthReq.macL1 = (DevMac&0x0000ff00)>>8;
    authMsg->routeAuthReq.macL0 = (DevMac&0x000000ff)>>0;
    authMsg->routeAuthReq.type = 1;

    outtime = 0;
    do
    {
      outtime++;
      result = ap_msg_auth_req(scanResult[minTreePos].routeIP,scanResult[minTreePos].ch,scanResult[minTreePos].dhcp,authMsg);
      if (result == 0)
      {
        DevInfoDef info = {0};

        //鉴权成功创建设备列表
        //dev_list_create(scanResult[minTreePos].sum);
        dev_list_create(1000);

        info.mac = DevMac;
        info.depthRoute = scanResult[minTreePos].tree+1;
        info.devtype= DevWapRoute;
        info.auth = Auth_Finsh;
        info.flag.devAuthFinsh = 1;//路由器鉴权完成。
        if(info.depthRoute < 1)
        {
          OS_ASSERT(NULL);
        }

        info.route[info.depthRoute-1] = scanResult[minTreePos].routeIP;
        info.route[info.depthRoute] = scanResult[minTreePos].dhcp;
        dev_list_node_set(scanResult[minTreePos].dhcp,&info);
        dev_list_show_node_mac(DevMac);
        break;
      }
    }while(outtime < 3);
    free(authMsg);
    
    if(outtime >= 3)//鉴权失败重新扫描
    {
      scan_wap_auth_process();
    }
  }
  else//没有一个鉴权成功的
  {
    scan_wap_auth_process();
    printf("T_T Scan Route Num =%d\n",scanNum);
  }
  
  return 1;
}

void *route_recv_thread_entry(void *arg)
{
  printf("route_thread_entry\n");

  while(1)
  {
    if(recvMsgThreadWork == 1)
    {
      int result;
      RxNrfMsgDef msg;

      result = get_nrf_recv_msg(&msg);//接收数据
      if(result == 0)//获取到有效的数据
      {
        route_nrf_msg_process(&msg);//接收报文处理
      }
    }
    else
    {
        system_delay_ms(10);
    }
  }
}






































static pthread_mutex_t msgSendMailMutex;//nrf2401邮件互斥锁
static int SendMsg;

static int msg_send_mail(MsgSendMailDef_p msg)
{
  int result;

  OS_ASSERT(msg);

  if(pthread_mutex_lock(&msgSendMailMutex)!=0) 
  {
    printf("nrf send mutex lock fail\n");
    OS_ASSERT(0);
  }
  msg->mtype = 1;

  result = msgsnd(SendMsg, (void *)msg, sizeof(NrfDevDef), 0);//发送nrf 邮件
  if(result == -1)
  {
    printf("send msg mail fail\n");
  }
  else
  {
    printf("send nrf msg ok\n");
  }

  if(pthread_mutex_unlock(&msgSendMailMutex)!=0) 
  {
    printf("nrf send mutex unlock fail\n");
    OS_ASSERT(0);
  }

  return result;
}

MsgSendMailDef recvData;

void event_send_result_process(int result,DevInfoDef_p info,int ip,unsigned char *flag)
{
      if(result == 0)//发送事件成功
      {
        dev_list_show_node(ip);
        (*flag) = 1;
        info->sendErrorCrl.errNum = 0;
      }
      else
      {
        if(info->sendErrorCrl.errNum < 1000)
        {
            info->sendErrorCrl.errNum++;
        }
        info->sendErrorCrl.delay = info->sendErrorCrl.errNum;
      }
      dev_list_node_set(ip,info);
}

void device_event_manage(void)
{
  int devSum;
  int linkNum;
  int pos;
  DevInfoDef_p info;
  unsigned char msgFlag = 0;
  routeEvtDef event = {0};
  NrfMsgDef *msg;//确认鉴权
  NrfMsgDef *recv;
  unsigned char priceStr[5];
  int selfIP;
  int devIP;
  unsigned char recvFlag = 0;

  msg =  calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv =  calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  selfIP = get_self_nrf_srcaddr();//获取自己的地址
  devSum = dev_list_sum_get();//获取设备列表中的总数量
  for(pos = 0; pos < devSum; pos++)
  {
    int result;
    devIP = pos+1;
    result = dev_list_node_get(devIP, info);//ip是下标加1的
    if(result != -1)//数据正常
    {
      /*
         1.设备自身不需要下发配置
         2.设备必须在线
         3.设备不是集中器
         4.设备的下级路由是设备自己。
         */
      if((info->auth == Auth_Finsh) && (DevMac != info->mac) && (info->devtype != DevWapCCOL))//在线就处理
      {
        if(info->sendErrorCrl.delay > 0)//通信出错推迟发送
        {
            info->sendErrorCrl.delay--;
            continue;
        }
        if(info->route[info->depthRoute+1] != devIP)//直接连在这个路由器上的路由器
        {
            if(info->flag.putDevPrice)//需要下发价格
            {
              printf("检测到mac%d 需要更新价格%d  flag= %x\n",info->mac,info->flag.putDevPrice,info->flag);
              msgFlag = 1;
              event.eventBit.UpdatePrice = 1;
            }
            if(info->flag.putDevConfig && info->flag.routeGetConfig == 0)//需要下发配置并且配置同步完毕。
            {
              if(info->transfer.flag == TRANSFE_NONE)//还没开始传输
              {
                msgFlag = 1;
                event.eventBit.UpdateConfig = 1;
              }
              else
              {
                printf("上一次传输未完成%d %d!!!\n",info->mac,msgFlag);
              }
            }
            if(msgFlag == 1)
            {
              float_to_u8(priceStr,info->price.data);//提取价格
              msg->routeEvtReq.cmd = MSG_RouteEvt_Req;//路由事件
              msg->routeEvtReq.ipH = (selfIP & 0xff00)>>8;
              msg->routeEvtReq.ipL = (selfIP & 0x00ff);
              msg->routeEvtReq.eventH1 = (event.eventU32 & 0xff000000)>>24;
              msg->routeEvtReq.eventH0 = (event.eventU32 & 0x00ff0000)>>16;
              msg->routeEvtReq.eventL1 = (event.eventU32 & 0x0000ff00)>>8;
              msg->routeEvtReq.eventL0 = (event.eventU32 & 0x000000ff)>>0;
              msg->routeEvtReq.priceH1 = priceStr[0];
              msg->routeEvtReq.priceH0 = priceStr[1];
              msg->routeEvtReq.priceL1 = priceStr[2];
              msg->routeEvtReq.priceL0 = priceStr[3];
              msg->routeEvtReq.devIPH = (devIP & 0xff00)>>8;
              msg->routeEvtReq.devIPL = (devIP & 0x00ff);
              msg->routeEvtReq.streamID = info->ramConfig.id;//用于校验两次流下发事件是否一样
              result = msg_send_dev(msg,recv,info->route[info->depthRoute+1], 0);//发送给下级设备
              event_send_result_process(result,info,devIP,&recvFlag);//分析发送结果
            }
        }

        if(info->flag.DevPriceOk)//下级设备更新价格成功，需要上报上级设备
        {
          if(RouteWorkMode == CCOL)
          {
            //通过网络报告服务器价格更新成功
            //info->flag.DevPriceOk = 0;
          }
          else//如果是路由器，需要通知上级设备
          {
            memset(msg,0,sizeof(NrfMsgDef));
            msg->REVTResultReq.cmd = MSG_REVTResult_Req;
            msg->REVTResultReq.ipH = (devIP & 0xff00)>>8;
            msg->REVTResultReq.ipL = (devIP & 0x00ff)>>0;
            msg->REVTResultReq.evtType = 3;
            msg->REVTResultReq.result = 1;//事件处理成功
            result = msg_send_dev(msg,recv,info->route[info->depthRoute-1], 0);//将更新事件通知到上一级路由或者集中器
            event_send_result_process(result,info,devIP,&recvFlag);//分析发送结果
          }
        }
        if(info->flag.DevConfigOk)
        {
          if(RouteWorkMode == CCOL)
          {
            //info->flag.DevConfigOk = 0;
          }
          else
          {
            memset(msg,0,sizeof(NrfMsgDef));
            msg->REVTResultReq.cmd = MSG_REVTResult_Req;
            msg->REVTResultReq.ipH = (devIP & 0xff00)>>8;
            msg->REVTResultReq.ipL = (devIP & 0x00ff)>>0;
            msg->REVTResultReq.evtType = 4;
            msg->REVTResultReq.result = 1;//事件处理成功
            result = msg_send_dev(msg,recv,info->route[info->depthRoute-1], 0);//将更新事件通知到上一级路由或者集中器
            event_send_result_process(result,info,devIP,&recvFlag);//分析发送结果
          }
        }
        if(recvFlag)//抽出时间处理来自各设备的应答
        {
           system_delay_ms(10);
        }
      }
    }
  }

  free(recv);
  free(info);
  free(msg);
}

void route_stream_transfer(void)
{
  int devSum;
  int linkNum;
  int pos;
  DevInfoDef_p info;
  unsigned char msgFlag = 0;
  routeEvtDef event = {0};
  NrfMsgDef *msg;//确认鉴权
  NrfMsgDef *recv;
  unsigned char priceStr[5];
  int selfIP;
  int devIP;
  unsigned char recvFlag = 0;

  msg =  calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(msg);

  recv =  calloc(1,sizeof(NrfMsgDef));
  OS_ASSERT(recv);

  info = calloc(1,sizeof(DevInfoDef));
  OS_ASSERT(info);

  selfIP = get_self_nrf_srcaddr();//获取自己的地址
  devSum = dev_list_sum_get();//获取设备列表中的总数量
  for(pos = 0; pos < devSum; pos++)
  {
    int result;
    devIP = pos+1;
    result = dev_list_node_get(devIP, info);//ip是下标加1的
    if(result != -1)//数据正常
    {
      /*
         1.设备自身不需要下发配置
         2.设备必须在线
         3.设备不是集中器
         4.设备的下级路由是设备自己。
         */
      if((info->auth == Auth_Finsh) && (DevMac != info->mac) && (info->devtype != DevWapCCOL)&&(info->devtype == DevPriceTag1))//在线就处理
      {
        if(info->transfer.flag == TRANSFE_DATA_RX)//流数据的接收方需要发送请求
        {
          memset(msg,0,sizeof(NrfMsgDef));
          msg->transferReq.cmd = MSG_StreamData_Req;
          msg->transferReq.ipH = (devIP & 0xff00)>>8;
          msg->transferReq.ipL = (devIP & 0x00ff);
          msg->transferReq.posH = (info->transfer.curPos & 0xff00)>>8;
          msg->transferReq.posL = (info->transfer.curPos & 0x00ff);
          msg->transferReq.streamID = info->transfer.id;
          result = msg_send_dev(msg,recv, info->route[info->depthRoute-1], 0);//像上级设备发送数据请求
          if(result == 0)
          {
              recvFlag = 1;
          }
          else
          {
              printf("send MSG_StreamData_Req Fail!!!\n");
              dev_list_show_node(devIP);
          }
        }
        else  if(info->transfer.flag == TRANSFE_START_RX)//设备进入向上级路由询问数据流请求状态
        {
          msg->streamReq.cmd = MSG_Stream_Req;
          msg->streamReq.ipH = (selfIP & 0xff00)>>8;
          msg->streamReq.ipL = (selfIP & 0x00ff)>>0;
          msg->streamReq.macH1 = (info->mac & 0xff000000)>>24;
          msg->streamReq.macH0 = (info->mac & 0x00ff0000)>>16;
          msg->streamReq.macL1 = (info->mac & 0x0000ff00)>>8;
          msg->streamReq.macL0 = (info->mac & 0x000000ff)>>0;
          msg->streamReq.posH = 0;
          msg->streamReq.posL = 0;
          msg->streamReq.type = 1;//传输配置文件
          msg->streamReq.id = 1;
          result = msg_route_send(msg, recv, -1, 0);//向上一级请求流传输
          if(result == 0)
          {
              recvFlag = 1;
          }
          else
          {
              printf("send MSG_Stream_Req Fail!!!\n");
              dev_list_show_node(devIP);
          }
        }
        else if(info->transfer.flag == TRANSFE_FINSH_RX)//当前设备下拉动作已经完成
        {
          if(info->flag.routeGetConfig == 1)//但还是不能退出下拉状态，说明请求事件集中器没有收到
          {
              if(info->flag.putDevConfig == 0)//配置以下拉成功但是应答发送失败了
              {
                  //事件发送失败
                msg->REVTResultReq.cmd = MSG_REVTResult_Req;
                msg->REVTResultReq.evtType = 2;
                msg->REVTResultReq.result = info->transfer.result;
                msg->REVTResultReq.ipH = (devIP& 0xff00)>>8;
                msg->REVTResultReq.ipL = (devIP & 0x00ff)>>0;
                result = msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//向上级路由器请求配置更新完成。
                if(result == 0)
                {
                  recvFlag = 1;
                }
                else
                {
                    printf("send MSG_REVTResult_Req Fail!!!\n");
                    dev_list_show_node(devIP);
                }
              }
          }
        }

        //鉴权通知
        if(info->flag.devAuthFinsh == 1)
        {
            msg->REVTResultReq.cmd = MSG_REVTResult_Req;
            msg->REVTResultReq.evtType = 5;//鉴权成功
            msg->REVTResultReq.result = info->transfer.result;
            msg->REVTResultReq.ipH = (devIP& 0xff00)>>8;//源地址
            msg->REVTResultReq.ipL = (devIP & 0x00ff)>>0;
            msg->REVTResultReq.uponIPH = (selfIP & 0xff00)>>8;
            msg->REVTResultReq.uponIPL = (selfIP & 0x00ff);
            result = msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//向上级路由器请求配置更新完成。
            if(result == 0)
            {
              recvFlag = 1;
            }
        }

        if(recvFlag)
        {
            recvFlag = 0;
            system_delay_ms(10);
        }
      }
      else if(info->devtype == DevWapRoute)//路由器设备检测一下是否有需要上报鉴权成功的
      {
        if(info->flag.devAuthFinsh == 1)
        {
           msg->REVTResultReq.cmd = MSG_REVTResult_Req;
            msg->REVTResultReq.evtType = 5;//鉴权成功
            msg->REVTResultReq.result = info->transfer.result;
            msg->REVTResultReq.ipH = (devIP& 0xff00)>>8;//源地址
            msg->REVTResultReq.ipL = (devIP & 0x00ff)>>0;
            msg->REVTResultReq.uponIPH = (selfIP & 0xff00)>>8;//自己的地址用于上级路由更新设备
            msg->REVTResultReq.uponIPL = (selfIP & 0x00ff);
            result = msg_send_dev(msg,recv,info->route[info->depthRoute-1],0);//向上级路由器请求配置更新完成。
            if(result == 0)
            {
                system_delay_ms(10);
            }
        }
      }
    }
  }

  free(recv);
  free(info);
  free(msg);
}


void route_send_msg_process(void)
{
  int result;
  static int i;

  i++;
  
  /* result = msgrcv(SendMsg,(void*)&recvData,sizeof(MsgSendMailDef),0,0);
     if (result > 0)
     {

     }*/
  //检测设备列表中的更新标志
  if(i % 100*3 == 0)
  {
    device_event_manage();
  }
  if(i%2 == 0)
  {
    route_stream_transfer();
  }
  
}

void test_price_change(void);
//路由发送线程
void *route_send_thread_entry(void *arg)
{
  //初始化发送邮件
  SendMsg = msget_method((const char *)__FUNCTION__);//发送nrf消息队列

  if(pthread_mutex_init(&msgSendMailMutex,NULL)!=0)  
  {
    printf("msg send mail mutex init fail !!!");
    sleep(1);
    OS_ASSERT(0);
  } 
  printf("input 0 CCOL 1 ROUTE\n");
  scanf("%d",&RouteWorkMode);

  printf("input device MAC:");
  scanf("%d",&DevMac);
  route_thread_init();
  //sleep(1);
  if(RouteWorkMode == CCOL)//集中器模式
  {
    DevInfoDef_p info;
    int dhcp;
    NrfDevDef nrfConfig;

    info = calloc(1,sizeof(DevInfoDef));
    OS_ASSERT(info);
    ccol_test_list_create();//集中器创建总路由表
    dhcp = dev_list_dhcp_get(DevMac,info);
    get_nrf_ap_config(&nrfConfig);
    set_nrfdev_srcaddr(&nrfConfig, dhcp);
    set_nrf_ap_config(&nrfConfig);
    nrf_16byte_mode();
    free(info);
  }
  else//路由器模式需要扫描
  {
    //route_test_list_create();//创建子路由表
    scan_wap_auth_process();//获取IP 通道号
    dev_list_show();
  }
  recvMsgThreadWork = 1;
  while(1)
  {
    route_send_msg_process();
    system_delay_ms(10);
    test_price_change();
  }
}



void test_price_change(void)
{
  static long i,j;
  static int mac_test[] = {37,38,40,41,42,43,44,45};
  static int testNnum = 0;
  i++;
  if(i % (100*300) == 0)
  {
    if(RouteWorkMode == CCOL)
    {
      int dhcp;
      DevInfoDef_p info;
      int packNum;

      info = calloc(1,sizeof(DevInfoDef));

      dhcp = dev_list_dhcp_get(mac_test[j++%(sizeof(mac_test)/sizeof(int))],info);
      info->price.data += 1.01;
      testNnum++;

      sprintf(info->ramConfig.data,"%d价格修改price =%08.02f UpdateID = %08d",testNnum,info->price.data,rand());
      info->ramConfig.size = strlen(info->ramConfig.data);
      info->ramConfig.id++;
      packNum = info->ramConfig.size / Tran_BUF_SIZE;
      if(info->ramConfig.size % Tran_BUF_SIZE)
      {
        packNum += 1;
      }
      info->ramConfig.crc16 = CRC16_2(packNum * Tran_BUF_SIZE, info->ramConfig.data);
      if(info->auth == Auth_Finsh)//鉴权之后改变价格
      {
        info->flag.putDevPrice = 1;
        info->flag.putDevConfig = 1;
        info->transfer.flag = 0;

        dev_list_node_set(dhcp,info);
      }
    }
  }
  // if(i % )
  {

  }

}











