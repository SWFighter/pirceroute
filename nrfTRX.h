#ifndef __NRFTRX_H___
#define __NRFTRX_H___
#include "appconfig.h"

#define NRF_ADDR_LEN			        3
#define NRF_8B_SIZE_TRX            8 //8 字节收发
#define NRF_16B_SIZE_TRX          16 //16 字节收发

#define LAN_MSG_FIX_LEN            32
#define NRF_SEND_ERROR            0XFF



/**
 * @brief       
 * nrf模块控制命令定义
 */
typedef enum
{
  NRF_SEND_RECV,      //发送并且接收
  NRF_ONLY_RECV,      //只接收
  NRF_SET_ADDR,       //设置地址
  NRF_8B_MODE,        //8字节模式
  NRF_16B_MODE,      // 16字节模式
  NRF_32B_MODE,       //32字节模式
}NRFCrlCmdDef;


/**
 * @brief       
 * 通讯错误定义
 */
typedef enum
{
  NRF_ERR_NONE=0,
  NRF_ERR_SOUTTIME,
  NRF_ERR_ROUTTIME,
  NRF_ERR_MSG,
  NRF_ERR_DATE,
}NRFErrorDef;


/**
 * @brief       
 * NRF数据通讯控制结构
 */
typedef struct 
{
  int                  mtype;
  int                   ch;									      //通道
  unsigned char   src_addr[NRF_ADDR_LEN]; 			//设备地址
  unsigned char   dst_addr[NRF_ADDR_LEN]; 			//wap地址
  unsigned char   data_len;						          //数据包长度
  unsigned int 	 wait_time;						        //发送完成后等待接收时间
  unsigned char   *send_buf;						        //发送数据缓存
  unsigned char   *recv_buf;						        //接收数据缓存
  sem_t              *finsh;							          //发送完成信号
  unsigned char   cmd;                          //用于做邮件不同类型处理的
}NrfDevDef;

typedef enum
{
   MSG_MACH = 0,
   MSG_MACL,
   MSG_CMD,
}NrfMsgByteDef;

//接收nrf报文定义
typedef struct
{
   long int mtype;
   unsigned char buf[LAN_MSG_FIX_LEN];
}RxNrfMsgDef;

int nrf24_trx_module_init(void);

void  get_nrf_ap_config(NrfDevDef *nrf);
void set_nrf_ap_config(NrfDevDef *nrf);
void *nrf24_tx_thread_entry(void *arg);
void *nrf24_rx_thread_entry(void *arg);
int get_nrf_recv_msg(RxNrfMsgDef *msg);//从recv收发器中拿出一条报文。
void set_nrfdev_srcaddr(NrfDevDef *dev,int addr);
void set_nrfdev_dstaddr(NrfDevDef *dev,int addr);
int get_nrfdev_srcaddr(NrfDevDef *dev);
int get_self_nrf_srcaddr(void);


#endif
