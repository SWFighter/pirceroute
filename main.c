#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include "nrfTRX.h"
#include "route.h"
#include "timer.h"

static int app_thread_create(void)
{
  pthread_t  pid1;
  pthread_t  pid2;
  pthread_t  pid3;
  pthread_t  pid4;
  pthread_t  pid5;

  int err;
  pthread_attr_t attr;
  struct sched_param sched;
  int rs;  
  int policy;

  rs = pthread_attr_init( &attr );
  OS_ASSERT( rs == 0 );

  policy = get_thread_policy(&attr);

  show_thread_priority(&attr, policy);

  set_thread_policy(&attr,SCHED_OTHER);
  show_thread_priority(&attr, SCHED_OTHER);
  
  printf("create thread ...\n");
  //�����߳�

  attr.__schedparam.sched_priority = 80;
  set_thread_policy(&attr,SCHED_OTHER);
  err = pthread_create(&pid1,&attr,nrf24_tx_thread_entry,NULL);
  if(err != 0)
  {
     printf("nrf24_tx_thread_entry create fail\n"); 
     return 1;
  }
  

  attr.__schedparam.sched_priority = 80;
  set_thread_policy(&attr,SCHED_OTHER);
  err = pthread_create(&pid2,&attr,nrf24_rx_thread_entry,NULL);
  if(err != 0)
  {
    printf("pid2 create fail\n"); 
    return 1;
  }
  
  attr.__schedparam.sched_priority = 80;
  set_thread_policy(&attr,SCHED_OTHER);
  err = pthread_create(&pid3,&attr,route_recv_thread_entry,NULL);
  if(err != 0)
  {
    printf("pid3 create fail\n"); 
    return 1;
  }

  attr.__schedparam.sched_priority = 80;
  set_thread_policy(&attr,SCHED_OTHER);
  err = pthread_create(&pid4,&attr,route_send_thread_entry,NULL);
  if(err != 0)
  {
    printf("pid4 create fail\n"); 
    return 1;
  }
  
  attr.__schedparam.sched_priority = 80;
  set_thread_policy(&attr,SCHED_OTHER);
  err = pthread_create(&pid5,&attr,timer_thread_entry,NULL);
  if(err != 0)
  {
    printf("pid5 create fail\n"); 
    return 1;
  }  
  pthread_join(pid1,NULL);  
  pthread_join(pid2,NULL);  
  pthread_join(pid3,NULL);
  pthread_join(pid4,NULL);
  pthread_join(pid5,NULL);


  printf("test app start run...\n");

  return 0;
}

int main(int argc, char **argv)
{
    nrf24_trx_module_init();
    app_thread_create();
    printf("############\n");
}

