#ifndef __ROUTLIST_H__
#define __ROUTLIST_H__
#include "appconfig.h"
#include "comm.h"

#define VAIN_DEV_ID             -1
#define CONFIG_BUF_SIZE     512


#define DEV_MAC_SIZE          32
#define LAN_IP_SIZE             32
#define ROUTE_GRADE          6
#define unAuth_List_SIZE       10
#define unAuth_OutTime        6000*2


enum DevTypeDef
{
  DevWapCCOL = 1,
  DevWapRoute,
  DevPriceTag1,
};

#if 0
typedef struct DevListDef
{
  struct DevListDef         *prev;
  struct DevListDef         *next;

  unsigned char               macStr[DEV_MAC_SIZE];        //设备的MAC地址
  unsigned char               ipStr[LAN_IP_SIZE];              //设备动态分配的IP地址
  long int                       mac;                                   //mac地址
  long int                       ip;                                       // 获取到的IP地址
  int                              devType;                            //设备类型
  unsigned char               auth;                                  //鉴权标志，为1表示允许鉴权，为0表示不能鉴权
  int                              route[ROUTE_GRADE];
  unsigned char               update;//检索更新的状态标记
  int                              selfTree;//路由器深度
}DevListDef;


void sysDev_list_init_process(void);


int get_list_dev_num(void);
DevListDef* sysDev_list_mac_find(long mac);
DevListDef* sysDev_list_head_get(void);

#endif
typedef struct 
{
    unsigned int putDevConfig:1;         // 0需要下发配置文件
    unsigned int putDevPrice:1;           // 1需要下发价格
    unsigned int routeConfigOK:1;       // 2表示下级路由更新配置完成
    unsigned int routePriceOk:1;         //  3表示下级更新价格完成
    unsigned int DevConfigOk:1;          // 4更新设备配置信息成功
    unsigned int DevPriceOk:1;            // 5更新设备价格信息成功
    unsigned int syncFinishFlag:1;        // 6表示设备的信息同步完成
    unsigned int routeGetConfig:1;      // 7路由器正在获取配置信息
    unsigned int devAuthFinsh:1;          // 8设备鉴权完成
    //unsigned int routeAuthFinsh:1;       //9 路由器鉴权完成
}bitFlagDef;

typedef struct
{
    unsigned char id;
    unsigned int   size;
    unsigned char data[CONFIG_BUF_SIZE];
    unsigned int crc16;
}streamDataDef;

//传输控制定义
typedef struct 
{
    unsigned char flag;//传输标志1请求流数据2传输流数据
    int                packNum;
    int                crc16;
    unsigned char id;
    int                curPos;
    unsigned char result;
}transferCrlDef;

typedef enum
{
  Auth_None,
  Auth_Ask,
  Auth_Finsh,
  Auth_No,
}authStatusDef;

typedef struct
{
   unsigned int errNum;//连续出错数量
   unsigned char delay;
   
}sendErrorCrlDef;

typedef struct
{
  long                          mac;                                      //mac地址
  int                            devtype;                                //设备类型
  unsigned char             auth;                                     //鉴权权限
  unsigned char             depthRoute;                          //路由深度
  bitFlagDef                  flag;
  unsigned int               route[ROUTE_GRADE];            //路由路径
  streamDataDef           ramConfig;      //配置文件缓存
  FloatUnionDef            price;
  transferCrlDef            transfer;//传输控制
  sendErrorCrlDef          sendErrorCrl;
}DevInfoDef,*DevInfoDef_p;
typedef struct 
{
    int                         num;        //数量
    DevInfoDef          node[10000];   //设备信息 
}DevListDef,*DevListDef_p;


//被拒绝的设备列表
typedef struct
{
  unsigned char flag;//是否为空
  unsigned int  outime;
  int                 mac;
}unAuthListDef,*unAuthListDef_p;


int dev_list_create(int num);
int dev_list_destroy(void);
int dev_list_node_set(int pos,DevInfoDef_p info);
int dev_list_node_get(int pos, DevInfoDef_p info);
void dev_list_show_node(int pos);
void dev_list_show(void);
int dev_list_node_get(int pos, DevInfoDef_p info);
int dev_list_num_get(void);
void dev_list_show_node_mac(int mac);





int unAuth_list_find_mac(int mac);
int get_unAuth_list_new_pos(void);
int add_unAuth_dev(int mac);
int unAuth_list_outTime_porcess(void);


#endif
