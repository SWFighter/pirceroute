#ifndef __APPHEAD_H__
#define __APPHEAD_H__
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <curses.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>		//终端控制定义
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include "string.h"
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include "crc16.h"

//#define Only_Route_Depth           1//指定连接一个路由深度从0开始

#define RF2401_DEBUG
#define WIFI_USING                  	1 //硬件有WIFI模块
#define CloseWatchDog


#define DES_KEY                         "12345678"

#ifdef WIFI_USING
#define UpdateOuttimeReboot          	(3601*12)
#define UpdateOuttimeGetDate         	(3602*6)
#define UpdateOuttimeFixData          	(3603*1)
#define UpdateOuttimeDevList           	(1804)
#define UpdateOuttimeCheckInfo1        	(605)
#define UpdateOuttimeCheckInfo2        	(1206)
#define UpdateOuttimeRTData           	(367)
#define UpdateOuttimePutRFCard      	(248)
#define UpdateOuttimeGetRFCard      	(150)
#define UpdateOuttimeConfigInfo      	(63)
#else
#define UpdateOuttimeReboot          	(3601*24*5)
#define UpdateOuttimeGetDate         	(3602*24)
#define UpdateOuttimeFixData          	(3603*12)
#define UpdateOuttimeDevList           	(3604*24*5)
#define UpdateOuttimeCheckInfo1      (605)
#define UpdateOuttimeCheckInfo2      (1206)
#define UpdateOuttimeRTData           	(3500*1)
#define UpdateOuttimePutRFCard      	(248*5)
#define UpdateOuttimeGetRFCard      	(150*5)
#define UpdateOuttimeConfigInfo      	(63*5)
#endif

#define Dev_None_Ack_Max_Num      2

#define RecvStreamBufSize                 1204






#define DebugInfoEnable
#define DEBUG_GSM_CSQ

#define RESEND_UPDATE_DATE        3

#define SystemConfigFilePath    "/tmp/system_info"
#define GSMLOG_FILE_PATH        "/tmp/gsminfo"
#define WA810_CONFIG_FILE       "/mnt/spiflash/wa810config"   //wa810 设备信息文件
#define WA810_MAC_SET 				"/mnt/spiflash/MacCheck"
#define DEVCONFIG_PATH			"/mnt/spiflash/devinfo_conifg"

#define CONFIG_CMD_LEN          32

#define GPIO_GPRS_LED			      100	
#define GPIO_LAN_LED				    6
#define GPIO_SYS_LED				    5
#define GPIO_WIFI_LED			      2

#define VAIN_DEV_ID             -1

typedef struct 
{
  unsigned char nrf_ch[CONFIG_CMD_LEN];
  unsigned char status[CONFIG_CMD_LEN];
  unsigned char host[CONFIG_CMD_LEN];
  unsigned char user[CONFIG_CMD_LEN];
}SysInfoFileDef;

typedef struct 
{
  SysInfoFileDef file;
  char               work_status;
}SystemInfoDef;
extern SystemInfoDef SystemInfo;

#define OS_ASSERT(EX)                                                    	\
if (!(EX))                                                               	\
{                                                                        	\
	volatile char dummy = 0;                                                \
	printf("(%s) assert failed at %s:%d \n", #EX, __FUNCTION__, __LINE__);	\
	exit(0);while (dummy == 0);                                             \
}


typedef enum
{
	DevInfoLine_NRFCH = 0,
	DevInfoLine_NetStatus,
	DevInfoLine_HostName,
	DevInfoLine_UserName,
	DevInfoLine_PWD,
}DevInfoConfigLineDef;

#define SYS_WLAN0_CHECK_SHELL        "ifconfig wlan0 | grep RUNNING -o" //wlan0 正常运行


#endif

