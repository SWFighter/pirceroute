#include "NRF24l01.h"

/* debugl  */
//#define  OS_USING_DEBUGE
#ifdef OS_USING_DEBUGE
#define dprintf			printf
#else
#define dprintf(...)   
#endif

#define NRF_SPI_DEV  "/dev/spidev0.2"		//NRF2401设备使用的spi
#define NRF_IQR_DEV "/dev/devirq"				//NRF2401设备的中断驱动

typedef signed char s8;
typedef unsigned char u8;

typedef signed short s16;
typedef unsigned short u16;

typedef signed int s32;
typedef unsigned int u32;

typedef signed long s64;
typedef unsigned long u64;

uint16_t IRQBegin = 0;
uint8_t	NRF_RxOKflag;
uint8_t	NRF_Rx_CMDbuf[8] = {0};

uint8_t	Pack_ffn = 0; //存储区内帧个数
uint8_t	NRF_PNUM = 0; //帧序号

struct pollfd nrf_irq_poll; //nrf2401的irq poll

uint8_t Wap_ch = 0;
uint8_t Cx_TestAd[3] = {0xA5,0xA5,0xff}; //设备1测试地址 [0][1]RF_addr;[2]SD_addr
uint8_t WAP_SweepAd[3] = {0xff,0XFF,0Xff};

//////////////////////////////////////////////////////////////////////////////////
/***
  24l01基本操作函数，不包括数据传输协议
 **/
//初始化通用参数

//启动NRF上电
/*void NRF_PowerUp(void)
  {
//配置上电
//首先进入配置模式
NRF24L01_Write_Reg(WRITE_REG_NRF+CONFIG,0x3f); //进入PWR_UP模式
delay_us(10);
}
*/

typedef struct 
{
  int fd;
  unsigned char mode;
  unsigned char  bits;
  unsigned int  speed;
  unsigned int delay; 
}spiconfig_def;

static spiconfig_def  nfr_spi = 
{
  0,
  0,
  8,
  8000000,
  100,
}; 
int nfr_spi_config(void)
{
  int ret = 0;

  dprintf("nfr_spi.mode = %d\n",nfr_spi.mode);
  dprintf("nfr_spi.delay = %d\n",nfr_spi.delay);
  dprintf("nfr_spi.bits = %d\n",nfr_spi.bits);
  dprintf("nfr_spi.speed = %d\n",nfr_spi.speed);
  //打开 spi  设备
  nfr_spi.fd = open(NRF_SPI_DEV, O_RDWR);
  if(nfr_spi.fd < 0)
  {
    dprintf("can't open device");
    exit(0);
  }

  //设置spi  模式
  ret = ioctl(nfr_spi.fd, SPI_IOC_WR_MODE, &nfr_spi.mode);
  if (ret == -1)
    dprintf("can't set spi mode");

  ret = ioctl(nfr_spi.fd, SPI_IOC_RD_MODE, &nfr_spi.mode);
  if (ret == -1)
    dprintf("can't get spi mode");

  //bits per word
  ret = ioctl(nfr_spi.fd, SPI_IOC_WR_BITS_PER_WORD, &nfr_spi.bits);
  if (ret == -1)
    dprintf("can't set bits per word");

  ret = ioctl(nfr_spi.fd, SPI_IOC_RD_BITS_PER_WORD, &nfr_spi.bits);
  if (ret == -1)
    dprintf("can't get bits per word");

  //max speed hz
  ret = ioctl(nfr_spi.fd, SPI_IOC_WR_MAX_SPEED_HZ, &nfr_spi.speed);
  if (ret == -1)
    dprintf("can't set max speed hz");

  ret = ioctl(nfr_spi.fd, SPI_IOC_RD_MAX_SPEED_HZ, &nfr_spi.speed);
  if (ret == -1)
    dprintf("can't get max speed hz");

  return ret;
}

static void transfer_buf(unsigned char *buf,int len)
{
  int ret;
  unsigned char *rx,*rx1;
  unsigned char *tx,*tx1;

  rx1 = rx = (unsigned char *)calloc(1,64);
  tx1 = tx =  (unsigned char *)calloc(1,64);
  OS_ASSERT(rx1);
  OS_ASSERT(tx1);

  memcpy(tx,buf,len);
  dprintf("\n>>>>>>\n");
  for (ret = 0; ret < len; ret++)
  {
    if (!(ret % 6))
      dprintf("");
    dprintf("%.2X ", tx[ret]);
  }
  struct spi_ioc_transfer tr = {
    .tx_buf = (unsigned long)tx,
    .rx_buf = (unsigned long)rx,
    .len = len,
    .delay_usecs = nfr_spi.delay,
    .speed_hz = nfr_spi.speed,
    .bits_per_word = nfr_spi.bits,
  };

  ret = ioctl(nfr_spi.fd, SPI_IOC_MESSAGE(1), &tr);
  if (ret < 1)
  {
		dprintf("can't send spi message");
		exit(-1);
  }
    

  dprintf("\n<<<<<<<\n");
  for (ret = 0; ret < len; ret++) {
    if (!(ret % 6))
      dprintf("");
    	//printf("%.2X ", rx[ret]);
  }
  dprintf("");

  memcpy(buf,rx,len);

  OS_ASSERT(rx1);
  OS_ASSERT(tx1);
  free(rx1);
  free(tx1);
}

void nrf_ce_config(void)
{
  FILE *gpio_fp; 
  char buffer[10];

  gpio_fp = NULL;
  dprintf("set ce gpio\n");
  if ((gpio_fp = fopen("/sys/class/gpio/export", "w")) == NULL)
  {
    dprintf("Cannot open export file.\n");
    return ;
  }
  else
  {
    fprintf(gpio_fp, "%d", 37);
    fclose(gpio_fp);
  }


  dprintf("set ce gpio direction\n");
  if ((gpio_fp = fopen("/sys/class/gpio/gpio37/direction", "rb+")) == NULL) 
  {			
    dprintf("/sys/class/gpio/gpio37/direction Cannot open direction file.\n");			
    return ;
  }		
  fprintf(gpio_fp, "out");		
  fclose(gpio_fp);
}

void NRF24L01_CE(int arg)
{
  FILE *gpio_fp; 
  char buffer[10];

  dprintf("set ce gpio value %d\n",arg);
  if ((gpio_fp = fopen("/sys/class/gpio/gpio37/value", "rb+")) == NULL)
  {
    dprintf("Cannot open value file.\n");
    return ;
  }
  else
  {
    if(arg == 1)
    {
      strcpy(buffer,"1");
    }
    else
    {
      strcpy(buffer,"0");
    }
    fwrite(buffer, sizeof(char), sizeof(buffer) - 1, gpio_fp);	
    fclose(gpio_fp);
  }
}

int NRF24L01_IRQ_Dev_Init(void)
{
	nrf_irq_poll.fd = open(NRF_IQR_DEV,O_RDWR);
    if (nrf_irq_poll.fd < 0)   
    {
    	printf("%s function open %s dev fail\n",__FUNCTION__,NRF_IQR_DEV); 
    	exit(-1);
    }
    nrf_irq_poll.events = POLLIN; //期待收到poll_in值，表示有数据
	return 0;
}

int NRF24L01_Wait_IRQ(int outtime)
{
	int ret;

	/* A value of 0 indicates  that the call timed out and no file descriptors were ready         
	  * poll函数返回0时，表示5s时间到了，而这段时间里，没有事件发生"数据可读"         
	  */
	ret = poll(&nrf_irq_poll,1,outtime);
	 if(ret != 0)      
	 {            
	 	read(nrf_irq_poll.fd,&ret,1);            
	 	dprintf("key_val = 0x%x\n",ret);       
	 }   
	
	return ret;
}

void NRF24L01_Found(void)
{
  u8 i;

  for(i = 0;i < 5; i++)
    if(NRF24L01_Check() == 0)
      break;

  if(i == 5)
  {
    printf("24L01 Check Failed!\n");
    usleep(1000);
    exit(1);
  }
  else
  {
    printf("24L01 Found !\n");
    //SetRXBegin();
  }
}

//检测24L01是否存在
//返回值:0，成功;1，失败
uint8_t NRF24L01_Check(void)
{
  uint8_t i, buf[5]={0XA5,0XA5,0XA5};

  //NRF_PowerUp();
  //delay_us(10);
  NRF24L01_Write_Buf(WRITE_REG_NRF+TX_ADDR,buf,3); //写入5个字节的地址.
  //delay_us(80);//若波特率为250K，则需要在这里添加延时，让波形编程变成标准的SPI
  dprintf("read TX_ADDR\n");
  NRF24L01_Read_Buf(TX_ADDR,buf,3); //读出写入的地址
  for(i=0;i<3;i++)
  {
  		dprintf("%X ",buf[i]);
		if(buf[i]!=0XA5)
		{
			break;
		}
  }
  if(i!=3)return 1; //检测24L01错误
  return 0; //检测到24L01
}
//SPI写寄存器
//reg:指定寄存器地址
//value:写入的值
unsigned char NRF24L01_Write_Reg(unsigned char reg,unsigned char value)
{
  unsigned char buf[3];

  buf[0] = reg;
  buf[1] = value;
  transfer_buf(buf,2);

  return buf[0];
}

//读取SPI寄存器值
//reg:要读的寄存器
unsigned char  NRF24L01_Read_Reg(unsigned char  reg)
{
  unsigned char buf[3];

  buf[0] = reg;
  buf[1] = 0XFF;
  transfer_buf(buf,2);

  return buf[1];
}

//在指定位置写指定长度的数据
//reg:寄存器(位置)
//*pBuf:数据指针
//len:数据长度
//返回值,此次读到的状态寄存器值
unsigned char NRF24L01_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned char len)
{
  unsigned char *buf,*buf1;
  unsigned char status;

  buf1 = buf = (unsigned char *)calloc(1,len+1);
  OS_ASSERT(buf1);
  
  buf[0] = reg;

  memcpy(&buf[1],pBuf,len);
  transfer_buf(buf,len+1);

  status = buf[0];
  
  free(buf1);

  return status;
}

//在指定位置读出指定长度的数据
//reg:寄存器(位置)
//*pBuf:数据指针
//len:数据长度
//返回值,此次读到的状态寄存器值
unsigned char NRF24L01_Read_Buf(unsigned char reg,unsigned char *pBuf,unsigned char len)
{
  unsigned char *buf;
  unsigned char status ;

  buf = (unsigned char *)calloc(1,len+1);
  OS_ASSERT(buf);
  
  memset(buf,0xff,len+1);
  buf[0] = reg;
  transfer_buf(buf,len+1);

  status = buf[0];
  memcpy(pBuf,&buf[1],len);

  OS_ASSERT(buf);
  free(buf);
  return status;
}

//成功0  失败1
uint8_t NRF_RxPacket(uint8_t * rxbuf,uint8_t rxsize,int wait_ms)
{
  uint8_t sta;
  int result;
  
  NRF24L01_Wait_IRQ(wait_ms);
  //wait_ms = 6;
  
  NRF24L01_CE(1);
  do
  {
    sta=NRF24L01_Read_Reg(STATUS); //读取状态寄存器的值
    if(sta&RX_OK) //接收到数据
    {
      NRF24L01_CE(0);
      NRF24L01_Read_Buf(RD_RX_PLOAD,rxbuf,rxsize); //读取数据
      NRF24L01_Write_Reg(FLUSH_RX,0xff); //清除TX FIFO寄存器
      NRF24L01_Write_Reg(WRITE_REG_NRF+STATUS,0x70); //清除TX_DS或MAX_RT中断标志
      NRF24L01_CE(1);
      return 0; //成功
    }
  }while(wait_ms--);
  return 1;
}

//成功0 失败 1
uint8_t NRF_TxPacket( uint8_t* txbuf,uint8_t txsize)
{
  volatile uint8_t sta = 0;
  uint16_t outtime=0;
  unsigned char TxR;
  //sta=NRF24L01_Read_Reg(STATUS); 
  //printf("Status Reg %02X\n",sta);
  NRF24L01_CE(0);
  NRF24L01_Write_Buf(WR_TX_PLOAD,txbuf,txsize); //填数
  //sta=NRF24L01_Read_Reg(STATUS); 
  //printf("Status Reg %02X\n",sta);
  NRF24L01_CE(1);
  //sta=NRF24L01_Read_Reg(STATUS); 
  //printf("Status Reg %02X\n",sta);
  NRF24L01_Wait_IRQ(20);
  NRF24L01_CE(0);
  sta=NRF24L01_Read_Reg(STATUS); //读取状态寄存器的值
  //TxR = NRF24L01_Read_Reg(OBSERVE_TX);
  //printf("TxR = %d\n",TxR>>4);
  NRF24L01_Write_Reg(WRITE_REG_NRF+STATUS,0x70); //清除TX_DS或MAX_RT中断标志
  NRF24L01_Write_Reg(FLUSH_TX,0xff);
  //printf("send status = %x\n",sta);
  if(sta & TX_OK)
  {
    //printf("NRF Send OK Status Reg = %02X\n",sta);
    return 0; //发送成功
  }
  else
  {
    //printf("NRF Send Fail Status Reg = %02X\n",sta);
    return 1; //发送失败
  }
}

void NRF_init(uint8_t nrf_ch)
{
  NRF24L01_CE(0);
  NRF24L01_Write_Reg(WRITE_REG_NRF+STATUS, 0X70);
  NRF24L01_Write_Reg(WRITE_REG_NRF+CONFIG,0x0f); //进入PWR_UP模式
  NRF24L01_Write_Reg(WRITE_REG_NRF+EN_AA,0x01); //使能通道0的自动应答
  NRF24L01_Write_Reg(WRITE_REG_NRF+EN_RXADDR,0x01); //使能通道0的接收地址
  NRF24L01_Write_Reg(WRITE_REG_NRF+SETUP_AW,1); //三位地址
  NRF24L01_Write_Reg(WRITE_REG_NRF+RF_CH,nrf_ch); //48
  //NRF24L01_CE(1);
  //usleep(100);
}

void nrf_init(uint8_t nrf_ch)
{
  unsigned char buf[] = 
  {
    WRITE_REG_NRF+CONFIG,0x3f,
    WRITE_REG_NRF+EN_AA,0x01,
    WRITE_REG_NRF+EN_RXADDR,0x01,
    WRITE_REG_NRF+SETUP_AW,1,
    WRITE_REG_NRF+RF_CH,20,
  };

  NRF24L01_CE(0);
  buf[8] = nrf_ch;
  transfer_buf(buf,10);
}

void NRF_setRXmode(uint8_t* Rx_Ad, uint8_t RX_width,uint8_t irqen)
{
  uint8_t buf[4];
  
  NRF24L01_CE(0);

  NRF24L01_Write_Reg(WRITE_REG_NRF+CONFIG,0x0f); //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式
  NRF24L01_Write_Buf(WRITE_REG_NRF+RX_ADDR_P0,Rx_Ad,ADR_WIDTH); //接收通道0
  NRF24L01_Write_Reg(WRITE_REG_NRF+RX_PW_P0,RX_width);
  NRF24L01_Write_Reg(WRITE_REG_NRF+RF_SETUP,0x27); //设置TX发射参数,0db增益,250kbps,低噪声增益开启

  NRF24L01_Write_Reg(WRITE_REG_NRF+STATUS, 0X70);
  NRF24L01_Write_Reg(FLUSH_RX,0xff);

  NRF24L01_CE(1);
  if(irqen) 
  {
    //usleep(130);
    system_delay_ms(50);
  }
  //NRF24L01_CE(0);
  //NRF24L01_Read_Buf(RX_ADDR_P0,buf,3);
  //printf("Nrf RX Set addr:%02X%02X%02X\n",buf[0],buf[1],buf[2]);
}

void NRF_setTXmode(uint8_t* Tx_Ad)
{
  uint8_t buf[4];
  
  NRF24L01_CE(0);

  NRF24L01_Write_Reg(WRITE_REG_NRF+CONFIG,0x0e); //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式,开启所有中断
  NRF24L01_Write_Buf(WRITE_REG_NRF+TX_ADDR,Tx_Ad,ADR_WIDTH); //设备发给
  NRF24L01_Write_Buf(WRITE_REG_NRF+RX_ADDR_P0,Tx_Ad,ADR_WIDTH); //ACK接收

  NRF24L01_Write_Reg(WRITE_REG_NRF+SETUP_RETR,0x13); //设置自动重发间隔时间:500us + 86us;最大自动重发次数:3次
  NRF24L01_Write_Reg(WRITE_REG_NRF+RF_SETUP,0x27); //设置TX发射参数,0db增益,250kbps,低噪声增益开启

  NRF24L01_Write_Reg(WRITE_REG_NRF+STATUS, 0X70);
  NRF24L01_Write_Reg(FLUSH_TX,0xff);
  
  //usleep(200);
  //NRF24L01_Read_Buf(TX_ADDR,buf,3); 
  //printf("Set NRF Addr:%02X%02X%02X\n",buf[0],buf[1],buf[2]);
  
}




/* end of file */

