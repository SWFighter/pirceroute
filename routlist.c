#include "routlist.h"

unAuthListDef   unAuthDevList[unAuth_List_SIZE] = {0};


DevListDef       sysDevList;


int dev_list_create(int num)
{
    if(num == 0)
    {
        printf("Create device list fail\n");
        return -1;
    }
    sysDevList.num = num;
    printf("sizeof(sysDevList) = %d MByte\n",sizeof(DevListDef)/1024/1024);
    //sysDevList.node = calloc(sysDevList.num,sizeof(DevInfoDef));
    //OS_ASSERT(sysDevList.node);

    return 0;
}

int dev_list_destroy(void)
{
    if(sysDevList.node != NULL)
    {
      sysDevList.num = 0;
      free(sysDevList.node);
    }
}

int dev_list_node_set(int pos,DevInfoDef_p info)
{
    pos -= 1;//ip
    if(pos >= sysDevList.num)
    {
        printf("set %d pos > list max num %d!!! \n",pos,sysDevList.num);
        return -1; 
    }
    if(info  == NULL)
    {
        printf("set list node add info is NULL  !!!\n");
        return -1;
    }

    memcpy(&sysDevList.node[pos],info,sizeof(DevInfoDef));

    return 0;
}

int dev_list_node_get(int pos, DevInfoDef_p info)
{
    pos -= 1;
    if(pos >= sysDevList.num)
    {
        printf("get %d pos > list max num %d!!! \n",pos,sysDevList.num);
        return -1; 
    }
    if(info  == NULL)
    {
        printf("get list node add info is NULL  !!!\n");
        return -1;
    }
    if(sysDevList.node[pos].mac == 0)
    {
      return -1;
    }

    memcpy(info,&sysDevList.node[pos],sizeof(DevInfoDef));
    return 0;
}

int dev_list_dhcp_get(int mac,DevInfoDef_p info)
{
    int i;

    for(i=0;i<sysDevList.num;i++)
    {
        if(sysDevList.node[i].mac == mac)
        {
            memcpy(info,&sysDevList.node[i],sizeof(DevInfoDef));
            break;
        }
        else
        {
        
        }
    }
    if(i >= sysDevList.num)
    {
        i = -1;
        return i;
    }

    return i+1;
}


void dev_list_show_node(int pos)
{
    pos -= 1;//ip
    if((sysDevList.node[pos].auth == Auth_Finsh)/*||(sysDevList.node[pos].auth == Auth_Ask)*/)
    {
        if(pos < sysDevList.num)
        {
            int i;
        
            printf("%d-------------------------------------------------------------------------------------------------------------------------------------------------------\n",pos+1);
            printf("| Device IP = %2d",pos+1);
            printf(" Mac = %2d",sysDevList.node[pos].mac);
            printf(" Type = %d",sysDevList.node[pos].devtype);
            printf(" Auth = %d",sysDevList.node[pos].auth);
            printf(" Depth = %d",sysDevList.node[pos].depthRoute);
            printf(" Price = %.2f ",sysDevList.node[pos].price.data);
            printf(" Flag = %x ",sysDevList.node[pos].flag);
            printf(" Transfer = %d ",sysDevList.node[pos].transfer.flag);
            
            printf("Route:");
            for(i=0;i<ROUTE_GRADE;i++)
            {
                printf("[%d]>>",sysDevList.node[pos].route[i]);
            }
            printf(" Config = %s ",sysDevList.node[pos].ramConfig.data);
            printf("\n");
        }
    }
}

void dev_list_show_node_mac(int mac)
{
    int i;

    for(i=0;i<sysDevList.num;i++)
    {
        if(sysDevList.node[i].mac == mac)
        {
            dev_list_show_node(i);
        }
    }
}

void dev_list_show(void)
{
    int i;

    for(i=0;i<sysDevList.num;i++)
    {
        dev_list_show_node(i);
    }
    printf("\n");
}

int dev_list_num_get(void)
{
    int i;
    int num = 0;
    
    for(i=0;i<sysDevList.num;i++)
    {
        if(sysDevList.node[i].auth == Auth_Finsh)
        {
            num++;
        }
    }

    return num;
}

int dev_list_sum_get(void)
{
    return sysDevList.num;
}
































int unAuth_list_find_mac(int mac)
{
    int i;
    
    for(i=0;i<unAuth_List_SIZE;i++)
    {
        if(unAuthDevList[i].mac == mac)
        {
            return i;
        }
    } 

    return -1;
}

int get_unAuth_list_new_pos(void)
{
    int i;
    
    for(i=0;i<unAuth_List_SIZE;i++)
    {
        if(unAuthDevList[i].flag == 0)
        {
            return i;
        }
    }

    //设备列表以及满了
    return -1;
}

int add_unAuth_dev(int mac)
{
    int pos;

    pos = unAuth_list_find_mac(mac);
    if(pos != -1)//如果能找到这个设备不能鉴权
    {
        return -1;
    }
    else
    {
        pos = get_unAuth_list_new_pos();//先找到一个空位置
        if(pos != -1)//有新位置
        {
            unAuthDevList[pos].flag = 1;
            unAuthDevList[pos].mac = mac;
            unAuthDevList[pos].outime = unAuth_OutTime;
            return pos;
        }
        else//设备列表已经满了
        {
            printf("UN Auth Device List None New Pos\n");
            return -1;
        }
    }
}

int unAuth_list_outTime_porcess(void)
{
    int i;

    for(i=0;i<unAuth_List_SIZE;i++)
    {
        if(unAuthDevList[i].flag == 1)
        {
            unAuthDevList[i].outime--;
            if(unAuthDevList[i].outime <= 0)//删除
            {
                unAuthDevList[i].flag = 0;
                unAuthDevList[i].mac = 0;
            }
        }
    }
}
