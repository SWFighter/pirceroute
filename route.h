#ifndef __ROUTE_H__
#define __ROUTE_H__
#include "appconfig.h"
#include "nrfTRX.h"

#define MAX_ROUTE_NUM   32
#define MAX_DHCP_NUM      1000
#define MAX_MSG_SIZE        16
#define Tran_BUF_SIZE        12//流传输每条报文数据缓存

typedef enum
{
    CCOL,//集中器模式
    ROUTE,//路由器模式
}RouteModeDef;

/*
typedef enum
{
    UpdateOk_Route
}UpdateOkTypeDef;*/

typedef enum
{
  MSG_AP_Auth_Req     = 0x01,
  MSG_AP_Auth_Ack      = 0x81,
  MSG_Stream_Req       = 0x02,
  MSG_Stream_Ack        = 0x82,
  MSG_StreamData_Req = 0x03,
  MSG_StreamData_ACK = 0x83,
  MSG_RouteEvt_Req     = 0x04,
  MSG_RouteEvt_Ack      = 0x84,
  MSG_Dev_Auth_Req     = 0x05,
  MSG_Dev_Auth_Ack      = 0x85,
  MSG_DevEvt_Req         =  0x06,
  MSG_DevEvt_Ack          =  0x86,
  MSG_UpdateOk_Req     =  0x07,
  MSG_UpdateOk_Ack      =  0x87,
  MSG_REVTResult_Req   = 0x08,
  MSG_REVTResult_Ack    = 0x88, 
}RouteMsgTypeDef;

typedef enum
{
    TRANSFE_NONE = 0,
    TRANSFE_START_TX = 1,
    TRANSFE_START_RX,
    TRANSFE_DATA_TX,
    TRANSFE_DATA_RX,
    TRANSFE_FINSH_TX,
    TRANSFE_FINSH_RX,
}transfeStatusDef;
//typedef 
typedef struct
{
    unsigned int UpdateConfig:1;
    unsigned int UpdatePrice:1;
    unsigned int QRCode:1;
    unsigned int DevReAuth:1;
}devEvtCharDef;

//设备的事件结构
typedef union
{
    devEvtCharDef  eventChar;
    unsigned int     eventU32;
}devEvtDef;

typedef struct
{
  unsigned int UpdateConfig:1;
  unsigned int UpdatePrice:1;
}routeEvtBitDef;

//路由事件结构
typedef union
{
  routeEvtBitDef eventBit;
  unsigned int      eventU32;
}routeEvtDef;

typedef struct 
{
   unsigned char ipH;
   unsigned char ipL;
   unsigned char cmd;
   //unsigned char no;
   unsigned char auth;//鉴权结果
   unsigned char devNUmH;          //接入设备的数量
   unsigned char devNumL;
   unsigned char dhcpH;               //鉴权设备获取的IP
   unsigned char dhcpL; 
   unsigned char depthRoute;        //路由深度
   unsigned char dstIPH;                 //总的设备数量
   unsigned char dstIPL;
   unsigned char devType;
   unsigned char MacH1;//设备地址
   unsigned char MacH0;
   unsigned char MacL1;
   unsigned char MacL0;
}RouteAuthAckDef;

typedef struct 
{
   unsigned char ipH;         //请求设备的IP地址
   unsigned char ipL;
   unsigned char cmd;
   //unsigned char no;
   unsigned char ch;
   unsigned char macH1;
   unsigned char macH0;
   unsigned char macL1;
   unsigned char macL0;
   unsigned char type;         //表示这条鉴权报文的类型
   unsigned char devType;
}RouteAuthReqDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char macH1;
  unsigned char macH0;
  unsigned char macL1;
  unsigned char macL0;
  unsigned char type;
  unsigned char id;
  unsigned char posH;
  unsigned char posL;
}StreamReqDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char packNumH;//包数量
  unsigned char packNumL;
  unsigned char crc16H;
  unsigned char crc16L;
  unsigned char error;
  unsigned char id;
  unsigned char macH1;
  unsigned char macH0;
  unsigned char macL1;
  unsigned char macL0;
}StreamAckDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char posH;
  unsigned char posL;
  unsigned char streamID;
}TransferReqDef;

typedef struct 
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;//传输过程中数据源更新cmd的bit6为应答为0xc3
  unsigned char result;
  unsigned char data[Tran_BUF_SIZE];
}TransferAckDef;

typedef struct 
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char eventH1;
  unsigned char eventH0;
  unsigned char eventL1;
  unsigned char eventL0;
  unsigned char priceH1;
  unsigned char priceH0;
  unsigned char priceL1;
  unsigned char priceL0;
  unsigned char devIPH;
  unsigned char devIPL;
  unsigned char streamID;//流数据ID
}RouteEvtReqDef;

typedef struct 
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char eventH1;
  unsigned char eventH0;
  unsigned char eventL1;
  unsigned char eventL0;
  unsigned char devIPH;
  unsigned char devIPL;
}RouteEvtAckDef;

typedef struct 
{
   unsigned char ipH;
   unsigned char ipL;
   unsigned char cmd;
   unsigned char ch;
   unsigned char macH1;
   unsigned char macH0;
   unsigned char macL1;
   unsigned char macL0;
   unsigned char type;
}DevAuthReqDef;

typedef struct
{
    unsigned char ipH;
    unsigned char ipL;
    unsigned char cmd;
    unsigned char result;
    unsigned char numH;
    unsigned char numL;
    unsigned char dhcpH;
    unsigned char dhcpL;
    unsigned char macH1;
    unsigned char macH0;
    unsigned char macL1;
    unsigned char macL0;
    unsigned char depth;
}DevAuthAckDef;


typedef struct 
{
    unsigned char ipH;
    unsigned char ipL;
    unsigned char cmd;
    unsigned char macH1;
    unsigned char macH0;
    unsigned char macL1;
    unsigned char macL0;
}DevEvtReqDef;

typedef struct 
{
    unsigned char ipH;
    unsigned char ipL;
    unsigned char cmd;
    unsigned char eventH1;//事件
    unsigned char eventH0;
    unsigned char eventL1;
    unsigned char eventL0;
    unsigned char priceH1;//价格
    unsigned char priceH0;
    unsigned char priceL1;
    unsigned char priceL0;
}DevEvtAckDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char evtType;
}UpdateOkReqDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char result;
  unsigned char evtType;//事件
}UpdateOkAckDef;

typedef struct
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char evtType;
  unsigned char result;
  unsigned char uponIPH;
  unsigned char uponIPL;
}RouteEvtResultReqDef;

typedef struct 
{
  unsigned char ipH;
  unsigned char ipL;
  unsigned char cmd;
  unsigned char result;
  unsigned char evtType;
}RouteEvtResultAckDef;
typedef union
{
   unsigned char                buf[LAN_MSG_FIX_LEN];
   RouteAuthAckDef           routeAuthAck;
   RouteAuthReqDef           routeAuthReq;
   StreamReqDef                streamReq;
   StreamAckDef                streamAck;                  
   TransferReqDef              transferReq;
   TransferAckDef               transferAck;
   RouteEvtReqDef             routeEvtReq;
   RouteEvtAckDef             routeEvtAck;
   DevAuthReqDef             devAuthReq;
   DevAuthAckDef             devAuthAck;
   DevEvtReqDef               devEvtReq;
   DevEvtAckDef               devEvtAck;
   UpdateOkReqDef           UpdateOkReq;
   UpdateOkAckDef           UpdateOkAck;
   RouteEvtResultReqDef   REVTResultReq;
   RouteEvtResultAckDef    REVTResultAck;
}NrfMsgDef,*NrfMsgDef_p;


//扫描路由的结果
typedef struct 
{
   int num;          //已经连接的数量
   int sum;          //设备总数
   int routeIP;              //发送方的ip地址(wap 的地址)
   int dhcp;         //获取到的Ip地址(自己获取的地址)
   int ch;             //通信的通道号
   int tree;          //路由深度
}ScanResultDef;



typedef struct 
{
  int                  mtype;
  NrfMsgDef        msg;
}MsgSendMailDef,*MsgSendMailDef_p;



void *route_recv_thread_entry(void *arg);
void *route_send_thread_entry(void *arg);

#endif

