.SUFFIXES : .x .o .c .s

ROOT = /usr/local/arm_linux_4.2/
INC :=$(ROOT)/arm-linux/include
LIB = $(ROOT)/lib/gcc/arm-linux/4.2.1
LIB1 =$(ROOT)/arm-linux/lib

CC=arm-linux-gcc -O0 -g -I$(INC) -static
WEC_LDFLAGS=-L$(LIB) -L$(LIB1)
STRIP=arm-linux-strip

TARGET = PriceTag
SRCS := main.c NRF24l01.c  comm.c nrfTRX.c route.c routlist.c timer.c crc16.c
#LIBS = -lgcc
LIBS = -lpthread

all: 
	$(CC) $(WEC_LDFLAGS) $(SRCS) -o $(TARGET) $(LIBS)
	$(STRIP) $(TARGET)
	cp $(TARGET)  /tftpboot/
clean:
	rm -f *.o 
	rm -f *.x 
	rm -f *.flat
	rm -f *.map
	rm -f temp
	rm -f *.img
	rm -f $(TARGET)	
	rm -f *.gdb
	rm -f *.bak
	rm -f *.c~
	rm -f *.h~
