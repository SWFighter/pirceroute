#include "nrfTRX.h"

#define OS_USING_DEBUGE
#ifdef OS_USING_DEBUGE
#define dprintf			printf
#else
#define dprintf(...)   
#endif


static pthread_mutex_t nrf24HWResMutex;//nrf2401 硬件资源互斥锁 
static pthread_mutex_t nrfSendMailMutex;//nrf2401邮件互斥锁

static int nrfRecvVailSize = NRF_16B_SIZE_TRX;

static int nrfSendMsg;
static int nrfRecvMsg;


NrfDevDef  NrfAPConfig = 
{
    1,          //消息队列硬性要求
    120,         //通道号                    
    {0,0,1},  //源地址
    {0,0,1},  //目的地址
    16,         //数据长度
    10,         //默认发送超时
    0,
    0,
    0,
    0,
};

void  get_nrf_ap_config(NrfDevDef *nrf)
{
    (*nrf) = NrfAPConfig;
}

void set_nrf_ap_config(NrfDevDef *nrf)
{
    NrfAPConfig = (*nrf);
}
/**
 * @brief set_nrf_recv_size       
 * 设置nrf接收固定数据的长度
 *
 * @param len
 */
void set_nrf_recv_size(int len)
{
    nrfRecvVailSize =  len;
}


/**
 * @brief get_nrf_recv_size       
 * 获取nrf接收固定数据的长度
 *
 * @return  
 */
int get_nrf_recv_size(void)
{
    return nrfRecvVailSize;
}

/**
 * @brief nrf24_res_lock      
 * nrf2401 资源上锁
 */
void nrf24_res_lock(void)
{
  if(pthread_mutex_lock(&nrf24HWResMutex)!=0) 
  {
    printf("nrf2401 HW res lock fail !!!");
    sleep(1);
    OS_ASSERT(0);
  }
}


/**
 * @brief nrf24_res_unlock      
 * nrf2401 资源解锁
 */
void nrf24_res_unlock(void)
{
  if(pthread_mutex_unlock(&nrf24HWResMutex)!=0) 
  {
    printf("nrf2401 HW res unlock fail !!!");
    sleep(1);
    OS_ASSERT(0);
  }
}

/**
 * @brief nrf24_trx_module_init       
 * nrf2401 收发器模块初始化
 *
 * @return  0 初始化成功 非0 初始化失败
 */
int nrf24_trx_module_init(void)
{
  int result = 0;
  NrfDevDef  nrfData ;
  

  //硬件初始化
  nfr_spi_config();
  nrf_ce_config();
  NRF24L01_IRQ_Dev_Init();
   NRF_init(NrfAPConfig.ch);
  //nrfData.str_addr[0]
  //nrf_recv_mode(&nrfData); 

  //初始化邮件
  nrfSendMsg = msget_method((const char *)__FUNCTION__);//发送nrf消息队列
  nrfRecvMsg = msget_method((const char *)__FUNCTION__);//接收nrf消息队列
  
  //初始化nrf2401硬件资源互斥锁
  if(pthread_mutex_init(&nrf24HWResMutex,NULL)!=0)  
  {
    printf("nrf2401 HW res mutex init fail !!!");
    sleep(1);
    OS_ASSERT(0);
  } 

  //初始化nrf2401 
  if(pthread_mutex_init(&nrfSendMailMutex,NULL)!=0)  
  {
    printf(" nrfSendMailMutex create fail\n");
    sleep(1);
    OS_ASSERT(0);
  }

  return result;
}

/**
 * @brief dev_id_to_int       
 * 将NRF的设备ID转换为一个整型数据返回
 * @param addr0    
 * @param addr1
 * @param addr2
 *
 * @return  
 */
int dev_id_to_int(int addr0, int addr1, int addr2)
{
  return (addr0*65536+addr1*256+addr2);
}


/**
 * @brief nrf_dev_show      
 * 显示NRF2401数据结构
 * @param dev
 */
void nrf_dev_show(NrfDevDef *dev)
{
  int i;

 /* dprintf("#################################################\n");
  dprintf("NRF2401 Info:\nCH=%d Len=%d Wait=%d DstAddr:%02x%02x%02x SrcAddr:%02x%02x%02x\n",
      dev->rf_ch,
      dev->data_len,
      dev->wait_time,
      dev->dst_addr[0],dev->dst_addr[1],dev->dst_addr[2],
      dev->src_addr[0],dev->src_addr[1],dev->src_addr[2]);*///hex

  dprintf("*******************************************************\nCH=%d Len=%d Wait=%d DstAddr:%02x%02x%02x[%d] SrcAddr:%02x%02x%02x[%d]\n",
      dev->ch,
      dev->data_len,
      dev->wait_time,
      dev->dst_addr[0],dev->dst_addr[1],dev->dst_addr[2],
      dev_id_to_int(dev->dst_addr[0], dev->dst_addr[1], dev->dst_addr[2]),
      dev->src_addr[0],dev->src_addr[1],dev->src_addr[2],
      dev_id_to_int(dev->src_addr[0], dev->src_addr[1], dev->src_addr[2]));      

   dprintf("bit    ",i);
   for(i=0;i<dev->data_len;i++)
   {
      dprintf("%2x ",i);
   }
   dprintf("\n");

  dprintf("Send>> ");
  for(i=0;i<dev->data_len;i++)
  {
    dprintf("%02x ",dev->send_buf[i]);
  }
  dprintf("\n");
  dprintf("Recv<< ");
  for(i=0;i<dev->data_len;i++)
  {
    dprintf("%02x ",dev->recv_buf[i]);
  }
  dprintf("\n");
}


/**
 * @brief nrf_send_msg      
 * 发送msg报文
 *
 * @param dev  报文节点
 *
 * @return  
 */
int nrf_send_msg(NrfDevDef *dev)
{
  int result = 0;

  OS_ASSERT(dev);
  OS_ASSERT(dev->send_buf);
  OS_ASSERT(dev->recv_buf);

  NRF_init(dev->ch);
  NRF_setTXmode(dev->dst_addr);
  result = NRF_TxPacket(dev->send_buf,dev->data_len);
  if(result == 1)
  {
    //printf("TX nrf error\n");
    dev->recv_buf[MSG_CMD] = NRF_SEND_ERROR;

    return result+1;
  }
  if(dev->wait_time == 0)
  {
	return result;
  }

  //NRF_init(dev->ch);
  NRF_setRXmode(dev->src_addr,dev->data_len,0);
  result = NRF_RxPacket(dev->recv_buf,dev->data_len,dev->wait_time);
  if(result == 0)
  {
    long addr;
    int i;

    addr = 0;
    for(i=0;i<NRF_ADDR_LEN;i++)
    {
        addr = addr |dev->dst_addr[i];
        addr <<= 8;
    }
    //printf("new addr %d\n\n",addr);
    //devm_list_dev_process(addr);
    //devm_list_show();
  }

  return result;
}

/**
 * @brief nrf_recv_mode       
 * 设置nrf进入接收模式，在发送完成后迅速进入接收模式
 *
 * @param dev
 *
 * @return  
 */
int nrf_recv_mode(NrfDevDef *dev)
{
  int result = 0;

  NRF_setRXmode(dev->src_addr,dev->data_len,0);

  return result;
}



//发送nrf邮件
int nrf_send_mail(NrfDevDef *dev)
{
  int result;

  OS_ASSERT(dev);

  if(pthread_mutex_lock(&nrfSendMailMutex)!=0) 
  {
    printf("nrf send mutex lock fail\n");
    OS_ASSERT(0);
  }
  dev->mtype = 1;
  //初始化信号量
  dev->finsh = calloc(1,sizeof(sem_t));
  OS_ASSERT(dev->finsh);

  result = sem_init(dev->finsh,0,0);
  if(result < 0)
  {
    printf("%S%d sem init fail\n",__FUNCTION__,__LINE__);
    OS_ASSERT(0);
  }
  
  result = msgsnd(nrfSendMsg, (void *)dev, sizeof(NrfDevDef), 0);//发送nrf 邮件
  if(result == -1)
  {
    printf("dev->ch = %d\n",dev->ch);
    printf("send nrf msg fail\n");
  }
  else
  {
      //dprintf("send nrf msg ok\n");
  }

  //等待发送完成
  sem_wait(dev->finsh);
  sem_destroy(dev->finsh);
  if(pthread_mutex_unlock(&nrfSendMailMutex)!=0) 
  {
    printf("nrf send mutex unlock fail\n");
    OS_ASSERT(0);
  }
  //销毁信号量
  free(dev->finsh);

  return result;
}



/**
 * @brief nrf24_tx_thread_entry
 * nrf2401 负责发送数据
 */
void *nrf24_tx_thread_entry(void *arg)
{
    int result;
    printf("nrf_tx_thread_entry\n");  
    
    while(1)
    {
      //接收发送nrf2401的发送邮件
      NrfDevDef data;

      result = msgrcv(nrfSendMsg,(void*)&data,sizeof(NrfDevDef),0,0);
      if(result > 0)
      {
          //锁定nrf2401硬件
          nrf24_res_lock();

          if(data.cmd == NRF_SET_ADDR)
          {
            NRF_setTXmode(data.dst_addr); //设置目的地址
            nrf_recv_mode(&data);	//进入接收状态
            //nrf_dev_show(&data);
          }
          else if(data.cmd == NRF_16B_MODE)//进入接收模式
          {
             set_nrf_recv_size(NRF_16B_SIZE_TRX);
             nrf_recv_mode(&data);
             //nrf_dev_show(&data);
          }
          else 
          {
            int result;
            
            result = nrf_send_msg(&data);
            nrf_recv_mode(&data);
            //if( result == 0 || result == NRF_SEND_ERROR)
            {
              nrf_dev_show(&data);
            }
          }
          OS_ASSERT(data.finsh);
          sem_post(data.finsh);  
          nrf24_res_unlock();
      }
      else
      {
          printf("%sRecv Error Send Mail\n",__FUNCTION__);
      }
    }
}


/**
 * @brief nrf24_rx_thread_entry       
 *等待处理接收无线数据包线程
 * @param arg
 *
 * @return  
 */
void *nrf24_rx_thread_entry(void *arg)
{
    printf("nrf24_rx_thread_entry \n");

    while(1)
    {
      int result;
      RxNrfMsgDef msg;

      nrf24_res_lock();

      memset(&msg,0,sizeof(RxNrfMsgDef));

      msg.mtype = 1;
      result = NRF_RxPacket(msg.buf,get_nrf_recv_size(),0);
      if(result == 0)
      {
         //监听到nrf 数据包
         msg.mtype = 1;
         result = msgsnd(nrfRecvMsg,(void *)&msg,sizeof(RxNrfMsgDef),0);
         if(result < 0)
         {
            printf("nrfRecvMsg send fail  !!!\n");
            OS_ASSERT(0);
         }
      }

      nrf24_res_unlock();
    }
}


int get_nrf_recv_msg(RxNrfMsgDef *msg)
{
  int result;
  RxNrfMsgDef data;
  int  i;
  
  result = msgrcv(nrfRecvMsg,(void *)&data,sizeof(RxNrfMsgDef),0,1);
  if(result == sizeof(RxNrfMsgDef))
  {
     result = 0;
     memcpy(msg,&data,sizeof(RxNrfMsgDef));
  }
  else
  {
     result = -1;
  }

  int fromip;
  fromip = u8_to_u16(data.buf[0],data.buf[1]);
  printf("\033[7;37;40m<<<<<<<< :");
  for(i=0;i<get_nrf_recv_size();i++)
  {
      if(i == 2)
      {
        if(data.buf[3] == 0x05 ||
           data.buf[3] == 0x07||
           data.buf[3] == 0x02 ||
           data.buf[3] == 0x03 ||
           data.buf[3] == 0x82 ||
           data.buf[3] == 0x83 )
        {
          printf("\033[7;30;47m%02X \033[7;37;40m",msg->buf[i]);
        }
        else
        {
          printf("\033[7;37;41m%02X \033[7;37;40m",msg->buf[i]);
        }
        
      }
      else
      {
        printf("%02X ",msg->buf[i]);
      }
  }
  printf("\033[0m\n");
  return result;
}



void set_nrfdev_srcaddr(NrfDevDef *dev,int addr)
{
    dev->src_addr[0] = ((addr & 0xff0000)>>16);
    dev->src_addr[1] = ((addr & 0x00ff00)>>8);
    dev->src_addr[2] = ((addr & 0x0000ff)>>0);
}

void set_nrfdev_dstaddr(NrfDevDef *dev,int addr)
{
  dev->dst_addr[0] = ((addr & 0xff0000)>>16);
  dev->dst_addr[1] = ((addr & 0x00ff00)>>8);
  dev->dst_addr[2] = ((addr & 0x0000ff)>>0);
}

int get_nrfdev_srcaddr(NrfDevDef *dev)
{
    int addr;

    addr = dev->src_addr[0]*65536+dev->src_addr[1]*256+dev->src_addr[2];

    return addr;
}

int get_self_nrf_srcaddr(void)
{
    NrfDevDef config;

    get_nrf_ap_config(&config);
    return get_nrfdev_srcaddr(&config);
}
