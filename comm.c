/**
 * @file comm.c
 * @brief 
 * 该文档提供工程中一些共用的功能函数
 * @author Fighter
 * @version 1.0
 * @date 2015-07-15
 */
#include "comm.h"
#include <string.h>



const unsigned char DebugColour[DebugColourNum][32] = 
{
    {"\033[0m"},
    {"\033[1;31;40m"},
    {"\033[1;32;40m"},
    {"\033[1;33;40m"},
    {"\033[1;34;40m"},
    {"\033[1;35;40m"},
    {"\033[1;36;40m"},
    {"\033[1;37;40m"},
};

#include <linux/watchdog.h>

static int watchdogFd = -1;

void clear_watchdog(void)
{
  #ifndef CloseWatchDog
  if(watchdogFd != -1)
  {
    ioctl(watchdogFd, WDIOC_KEEPALIVE);
  }

  #endif
}

void open_watchdog(void)
{
  #ifndef CloseWatchDog
  system("echo -n V > /dev/watchdog");
  //system_delay_ms(200);
  watchdogFd = open("/dev/watchdog", O_WRONLY);
  if (watchdogFd == -1) 
  {   
    perror("watchdog");  
    system("echo -n V > /dev/watchdog");
    exit(-1); 
  }
  else
  {
    printf("Open watchdog ok\n");
  }
  #endif
}


/**
 * @brief msget_method      
 * 创建msg方法
 * @param flag
 *
 * @return msg 
 */
int msget_method(const char *funname)
{
  int msg;
  
  msg = msgget(IPC_PRIVATE,0777|IPC_CREAT);
  if(msg < 0)
  {
    printf("create %s msg  fail %d\n",funname,msg);
    printf("%m", errno);
    exit(0);
  }
  else
  {
    printf("create %s msg %d\n",funname,msg);
  }
  return msg;
}

/**
 * @brief system_time_get       
 * 获取系统时间
 *
 * @param date   时间结构
 */
void system_time_get(struct tm **date)
{
  time_t timep;

  time(&timep);
  *date=localtime(&timep);
}

int system_info_set_nrfch(int ch)
{
  //sprintf(SystemInfo.file.nrf_ch ,"%02d\n",ch);
}

int system_info_get_nrfch(void)
{
  int ch;
  
  //sscanf(SystemInfo.file.nrf_ch,"%02d",&ch);

  return ch;
}

int   system_info_set_username(char *data)
{
	//sprintf(SystemInfo.file.user,"%s\n",data);
}

int system_info_set_hostname(char *data)
{
	//sprintf(SystemInfo.file.host,"%s\n",data);
}

int system_info_set_status(int staus)
{
/*  memset(SystemInfo.file.status,0,CONFIG_CMD_LEN);
  switch(staus)
  {
      case 0://
      {
        sprintf(SystemInfo.file.status ,"%s\n","0");
        break;
      }
      case 1:
      {
        sprintf(SystemInfo.file.status ,"%s\n","1");
        break;
      }
      case 2:
      {
        sprintf(SystemInfo.file.status ,"%s\n","1");
        break;
      }
      case 3:
      {
        sprintf(SystemInfo.file.status ,"%s\n","1");
        break;
      }
      case 4:
      {
        sprintf(SystemInfo.file.status ,"%s\n","0");
        break;
      }
      case 5:
      {
        sprintf(SystemInfo.file.status,"%s\n","2");
        break;
      }
      case 6:
      {
        sprintf(SystemInfo.file.status,"%s\n","3");
        break;
      }
      default:
      {
        
      }
  }*/
}

int system_info_show(void)
{
 /* printf("%s%s%s%s%s%s%s%s\n",
           SystemInfo.file.nrf_ch[0],SystemInfo.file.nrf_ch[1],
           SystemInfo.file.status[0],SystemInfo.file.status[1],
           SystemInfo.file.swv[0],SystemInfo.file.swv[1],
           SystemInfo.file.hwv[0],SystemInfo.file.hwv[1]);*/
}

int system_info_file_update(void)
{
/*	FILE *fp;
	int cnt=0;
	char str[100]="\0";

	fp=fopen(SystemConfigFilePath,"w");
	if(fp==NULL) //如果失败了
	{
	    exit(0);
	}
	fputs(SystemInfo.file.nrf_ch,fp);

	system_info_set_status(SystemInfo.work_status);
	fputs(SystemInfo.file.status,fp);

	fputs(SystemInfo.file.host,fp);

	fputs(SystemInfo.file.user,fp);

	fclose(fp); //关闭文件*/
}


void system_delay_ms(int time)
{
  struct timeval delay;
  delay.tv_sec = 0;
  delay.tv_usec = (time * 1000); // 100 ms
  select(0, NULL, NULL, NULL, &delay);
}


int run_shell_modlue(char *cmd,char *buf,long size)
{
  FILE   *stream; 

  memset( buf, '\0', sizeof(buf) );//初始化buf,以免后面写如乱码到文件中
  stream = popen( cmd, "r" ); //将“ls －l”命令的输出 通过管道读取（“r”参数）到FILE* stream
  if(stream != NULL)
  {
    fread( buf, sizeof(char),size, stream); //将刚刚FILE* stream的数据流读取到buf中
    pclose( stream ); 

    return 0;
  }
  else
  {
     return -1;
  }
}


static int SysWlan0Status;

int sys_wlan0_status(void)
{
  int result;

  result = SysWlan0Status;

  return result;
}

int get_wlan0_run(void)
{
  char *buf;
  int result = 0;
  
  buf = calloc(1,128);
  OS_ASSERT(buf);
  if(run_shell_modlue(SYS_WLAN0_CHECK_SHELL,buf,128) == 0)
  {
      if(strstr(buf,"RUNNING") != NULL)
      {
         result = 0;
         system("killall devconfig");
      }
      else
      {
        result = -1;
      }
  }
  else
  {
      result = -1;
  }
  free(buf);

  SysWlan0Status = result;
  return result;
}


/*void set_gpio_pin(int pin,int dat)
{
	FILE *fp;
	char filename[128];
	char buffer[10];
	
	if ((fp = fopen("/sys/class/gpio/export", "w")) == NULL) 
	{			
		printf("Cannot open export file.\n");	
		return ;
		//exit(1);		
	}	
	fprintf(fp, "%d",pin);		
	fclose(fp);

	memset(filename,0,128);		
	sprintf(filename,"/sys/class/gpio/gpio%d/direction",pin);		
	//printf("file name :%s\n",filename);		
	// equivalent shell command "echo out > direction" to set the port as an input  		
	if ((fp = fopen(filename, "rb+")) == NULL) 
	{		
		printf("%sCannot open direction file.\n",filename);		
		return ;
		//exit(1);	
	}		
	fprintf(fp, "out");		
	fclose(fp);

	memset(filename,0,128);			
	sprintf(filename,"/sys/class/gpio/gpio%d/value",pin);			
		
	if ((fp = fopen(filename, "rb+")) == NULL)
	{				
		printf("Cannot open value file.\n");		
		return ;
		//exit(1);		
	} 
	else 
	{			
		if(dat)		
			strcpy(buffer,"1");		
		else			
			strcpy(buffer,"0");		
		thread_mux_lock(&GpioWriteMux);
		fwrite(buffer, sizeof(char), sizeof(buffer) - 1, fp);		
		thread_mux_unlock(&GpioWriteMux);
		fclose(fp);		
	}
}*/



int get_thread_policy( pthread_attr_t *attr )
{
        int policy;
        int rs = pthread_attr_getschedpolicy(attr, &policy );
        OS_ASSERT( rs == 0 );
        switch(policy)
        {
        case SCHED_FIFO:
                printf("policy = SCHED_FIFO\n" );
                break;

        case SCHED_RR:
                printf("policy = SCHED_RR\n" );
                break;

        case SCHED_OTHER:
                printf("policy = SCHED_OTHER\n" );
                break;

        default:
                printf("policy = UNKNOWN\n" );
                break;
        }

        return policy;
}

void set_thread_policy( pthread_attr_t *attr,  int policy )
{
        int rs = pthread_attr_setschedpolicy(attr, policy );
        OS_ASSERT( rs == 0 );
        get_thread_policy( attr );
}

void show_thread_priority( pthread_attr_t *attr, int policy )
{
        int priority = sched_get_priority_max(policy);
        OS_ASSERT( priority != -1 );
        printf( "max_priority = %d\n",priority);

        priority = sched_get_priority_min(policy );
        OS_ASSERT( priority != -1 );
        printf( "min_priority = %d\n",priority);
}

long buf_u8_to_u32(char *buf)
{
  long mac;
  
  mac = buf[0]*16777216+buf[1]*65536+buf[2]*265+buf[3];

  return mac;
}

long u8_to_u32(unsigned char h1,unsigned char h0,unsigned char l1,unsigned char l2)
{
  long mac;
  
  mac = h1*16777216+h0*65536+l1*256+l2;

  return mac;
}

int u8_to_u16(unsigned char h,unsigned char l)
{
    int data;

    data = h*256+l;
    
    return data;
}

float u8_to_float(unsigned char h1,unsigned char h0,unsigned char l1,unsigned char l0)
{
    FloatUnionDef unionData;

    unionData.buf[0] = h1;
    unionData.buf[1] = h0;
    unionData.buf[2] = l1;
    unionData.buf[3] = l0;
    printf("%x %x %x %x data.data = %f\n",unionData.buf[0],unionData.buf[1],unionData.buf[2],unionData.buf[3],unionData.data);

    return unionData.data;
}

void float_to_u8(unsigned char *bufch,float raw)
{
    FloatUnionDef unionData;

    unionData.data = raw;
    memcpy(bufch,unionData.buf,4);
}


